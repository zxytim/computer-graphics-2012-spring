#
# $File: Makefile
# $Date: Wed Jun 20 19:43:49 2012 +0800
#
# A single output portable Makefile for
# simple c++ project

OBJ_DIR = obj
BIN_DIR = bin
TARGET = ray-tracing

IMG_SIZE =  -s 1280x800 #320x200 #1920x1200 #320x200 #1920x1080 #-s 640x400 #1280x800 #200x150 #800x600
RENDER_PARAM =  -s 800x600 -d 1 -k 0 -n 4 -o kd-tree-test.png -a 0 -l 0 -#-l 1 -d 10 -k 1 # -l 0 -k 1 -d 1 

BIN_TARGET = $(BIN_DIR)/$(TARGET)

PKGCONFIG_LIBS = Magick++
INCLUDE_DIR = -I src/include -I src -I src/lib
DEFINES = -D__DEBUG_BUILD  #-D__SIMPLIFIER_TEST # -D__DEBUG_TEST_KD_TREE

CXXFLAGS += -O3 #-g -O0 #-pg
CXXFLAGS += #$(DEFINES)
CXXFLAGS += -std=c++11
CXXFLAGS += -Wall -Wextra 
CXXFLAGS += $(INCLUDE_DIR) \
		   $(shell pkg-config --cflags $(PKGCONFIG_LIBS)) \
		   $(shell Magick++-config --cxxflags --cppflags) 

LDFLAGS = -lpthread -lrt\
		  $(shell pkg-config --libs $(PKGCONFIG_LIBS)) \
		  $(shell Magick++-config --ldflags --libs)

CXX = g++
CXXSOURCES = $(shell find src -name "*.cxx")
OBJS = $(addprefix $(OBJ_DIR)/,$(CXXSOURCES:.cxx=.o))
DEPFILES = $(OBJS:.o=.d)

.PHONY: all clean run rebuild gdb

all: $(BIN_TARGET)

$(OBJ_DIR)/%.o: %.cxx
	@echo "[cxx] $< ..."
	@$(CXX) -c $< $(CXXFLAGS) -o $@

$(OBJ_DIR)/%.d: %.cxx
	@mkdir -pv $(dir $@)
	@echo "[dep] $< ..."
	@$(CXX) $(INCLUDE_DIR) $(CXXFLAGS) -MM -MT "$(OBJ_DIR)/$(<:.cxx=.o) $(OBJ_DIR)/$(<:.cxx=.d)" "$<" > "$@"

sinclude $(DEPFILES)

$(BIN_TARGET): $(OBJS)
	@echo "[link] $< ..."
	@mkdir -p $(BIN_DIR)
	@$(CXX) $(OBJS) -o $@ $(LDFLAGS) $(CXXFLAGS)

clean:
	rm -rf $(OBJ_DIR) $(BIN_DIR)
	cd doc/manual/ && make clean
	cd doc/ray-tracing/ && make clean

run: $(BIN_TARGET)
	./$(BIN_TARGET) --show $(IMG_SIZE) $(RENDER_PARAM)

nokd: $(BIN_TARGET)
	./$(BIN_TARGET) --show $(IMG_SIZE) $(RENDER_PARAM) -k 0


rebuild:
	+@make clean 
	+@make

gdb: $(BIN_TARGET)
	gdb ./$(BIN_TARGET)

show: 
	geeqie output.png


# --show -s 300x225  -l 0 -k 1 -d 1 -n 1
