/*
 * $File: scene.cxx
 * $Date: Tue Jun 19 10:05:30 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "scene.hxx"


int Scene::width() const
{ return image_renderer.width(); }

int Scene::height() const
{ return image_renderer.height(); }

Scene::Scene()
	: image_renderer(1280, 800),
	view_pos(0, 0, 0), view_dir(0, 0, -1), up_dir(0, 1, 0),
	ambient(0.1, 0.1, 0.1),
	enable_shadow(false),
	anti_aliasing(AA_NONE),
	aa_cfaa_coef(0.075),
	enable_medium_density(true),
	frustum(-12.8, 12.8, -8, 8, 20)
{
}

Scene::~Scene()
{
}


void Scene::addRenderable(RenderablePrimitive *obj)
{
	renderable.push_back(obj);
}

void Scene::addLightSource(const LightSource &light)
{
	light_source.push_back(light);
}

void Scene::addLightSource(real_t x, real_t y, real_t z,
				real_t red, real_t green, real_t blue)
{
	addLightSource(LightSource(x, y, z, red, green, blue));
}

void Scene::addLightSource(real_t x, real_t y, real_t z,
		const Intensity &intensity)
{
	addLightSource(LightSource(x, y, z, intensity));
}

void Scene::setView(const Vector &view_pos, const Vector &view_dir,
		const Vector &up_dir)
{
	this->view_pos = view_pos;
	this->view_dir = view_dir;
	this->up_dir = up_dir;

	this->view_dir.unitize();
	this->up_dir.unitize();

	assert(fabs(this->view_dir.dot(this->up_dir)) < EPS);
}

void Scene::setImageResolution(int width, int height)
{
	image_renderer.resize(width, height);
}

void Scene::setFrustum(real_t left, real_t right,
		real_t bottom, real_t top, real_t dist)
{
	frustum = Frustum(left, right, bottom, top, dist);
}

void Scene::setShadow(bool enable_shadow)
{
	this->enable_shadow = enable_shadow;
}

void Scene::setAmbient(real_t red, real_t green, real_t blue)
{
	this->ambient = Intensity(red, green, blue);
}

void Scene::setAmbient(const Intensity &ambient)
{
	this->ambient = ambient;
}


void Scene::setAntiAliasing(int type)
{
	this->anti_aliasing = type;
}

void Scene::setCFAACoefficient(real_t coef)
{
	this->aa_cfaa_coef = coef;
}

void Scene::setEnableMediumDensity(bool state)
{
	enable_medium_density = state;
}

void Scene::display()
{
	image_renderer.display();
}

void Scene::write(const char *fname)
{
	image_renderer.write(fname);
}

void Scene::clearRenderable()
{
	renderable.clear();
}

