/*
 * $File: raytracing.hxx
 * $Date: Fri Jun 08 16:44:22 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */


#ifndef __HEADER_RAY_TRACING__
#define __HEADER_RAY_TRACING__

#include "common.hxx"
#include "scene.hxx"
#include <set>
#include <queue>
#include <algorithm>
#include <kdtree.hxx>

namespace RayTracingImpl
{
	void *rayTracingThreadProxy(void *arg);
}
/**
 * The RayTracing base
 *
 * World Coordinate Settings:
 *		Right Handed Coordinate system is used(the same as OpenGL):
 *				x-axis: point right
 *				y-axis: point up
 *				z-axis: point out
 */
class RayTracing
{
	protected:

		int trace_depth_max;
		real_t min_trace_energy;

		int nthread;
		int enable_kdtree;

		Intensity rayTracing(Ray &ray, 
			   	real_t refractive_index,
			   	Intensity energy, 
				real_t medium_density,
				int ninside,
				int depth, Scene &scene);

		static Vector getReflect(
				const Vector &incidence,
				const Vector &normal);

		static Vector getRefract(
				const Vector &incidence,
				const Vector &normal,
				real_t rindex_in,
				real_t rindex_out);

		int npixels;
		int last_percent;

		Intensity *image_buffer;

		// pixels needed to be rendered
		int rpixel_total;
		int nrendered_pixel;

		pthread_mutex_t render_mutex;

		long long start_time;

		friend void *RayTracingImpl::rayTracingThreadProxy(void *arg);
		void rayTracingThread(Scene &scene, bool show_log);

		void create_tracing_threads(int nthread, bool show_log, pthread_t  *thread);
		void wait_tracing_threads(int nthread, pthread_t *thread);

		struct RenderPixelTask
		{
			real_t x, y;
			Intensity *result;
		};
		std::queue<RenderPixelTask> render_task_queue;
		// for Anti-Aliasing

		Scene *scene;

		KdTree<RenderablePrimitive, RayTask> kdtree;

	public:

		RayTracing();
		~RayTracing();

		void setTraceDepth(int depth);

		// ranging from 0 to 1
		// follow Beer's law
		// to add the effect of light absorption of
		// material
		//
		// which can be expressed by:
		//	 light_out = light_in * e^(-d * C)
		// where d is path length through this material,
		// and C is a material specified constant
		void setMinTraceEnergy(real_t energy);

		void setThreadNumber(int nthread);
		void setKdTree(int status);

		void render(Scene &scene);
};

#endif
