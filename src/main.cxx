/*
 * $File: main.cxx
 * $Date: Wed Jun 20 19:48:55 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include <cstdlib>
#include <cstdio>
#include <string>
#include <image.hxx>
#include <raytracing.hxx>
#include <cstdlib>
#include <getopt.h>

#include <obj.hxx>

#include <vector>
#include <sstream>

using std::vector;
using std::string;

string itoa(int num)
{
	std::stringstream ss;
	ss << num;
	string s;
	ss >> s;
	return s;
}

string ssprintf(const char *fmt, ...)
{
	va_list args; 
	va_start(args, fmt); 
	const int STR_LEN_MAX = 1000;
	char msg_buf[STR_LEN_MAX];
	vsnprintf(msg_buf, STR_LEN_MAX - 1, fmt, args); 
	va_end(args); 
	return msg_buf;
}


AutoPtr<Mesh> loadMeshFromObjFile(std::string fname)
{
	fprintf(stderr, "loading mesh from file `%s' ... ", fname.c_str());
	std::vector<ObjParser::Vertex> ovtx;
	std::vector<ObjParser::Face> oface;
	ObjParser::read(fname, ovtx, oface);

	AutoPtr<Mesh> mesh(new Mesh());

	for (auto &v: ovtx)
	{
		if (v.has_normal)
			mesh->addVertexN(v.x, v.y, v.z,
					v.n_x, v.n_y, v.n_z);
		else mesh->addVertex(v.x, v.y, v.z);
	}
	for (auto &f: oface)
		mesh->addFace(f.v0, f.v1, f.v2);

	fprintf(stderr, "nvtx=%d, nface=%d\n", mesh->nvtx(), mesh->nface());
	return mesh;
}


void printMeshToObjFile(Mesh *mesh, const char *fname)
{
	FILE *file = fopen(fname, "w");

	mesh->printToObjFile(file);

	fclose(file);
}

std::string progname;
void print_usage()
{
	printf(
			"Usage: %s [options]\n"
			"Options: \n"
			"     --help, -h           print this help and exit\n"
			"     --shadow=NUM, -l 0 for no shadow, 1 for point light source\n"
			"                          shadow, 2 for 5x5 soft shadow, default=1\n"
			"     --anti-aliasing=NUM, -a 0 for no AA, 1 for 2x CFAA with coefficient 0.1,\n"
			"                          2 for 2x FSAA. If AA enabled, original un-AA image\n"
			"                          will be saved to `original.png`, if FSAA enabled, \n"
			"                          additional CFAA image will be saved to `cfaa.png` \n"
			"                          and a 4 times large un-AA image will be saved to \n"
			"                          `fsaa-used-2x.png`\n"
			"     --medium-density, -m 0 for off, 1 for on\n"
			"     --trace-depth=NUM, -d specify the maximum depth raytracing processes\n"
			"                          default=15\n"
			"     --nthread=NUM, -n    specify number of threads for rendering\n"
			"     --output=FILE, -o    specify output file, default is `output.png`\n"
			"     --size=SIZE, -s      specify size of the image, default is `1920x1080`\n"
			"     --show,              show the image right after rendering finished.\n"
			"     --kd-tree=NUM, -k    switch for kd-tree speed up, 0 for disable, \n"
			"                          1 for enable\n"
			,
		progname.c_str());
}

struct MeshConf
{
	std::string fname;
	std::string picname;

	real_t width, height;

	real_t ztrans;
	real_t length;
	real_t rdeg_x, rdeg_y, rdeg_z;


	MeshConf(std::string fname, std::string picname,
			real_t width, real_t height, 
			real_t ztrans, real_t length,
			real_t rdeg_x = 0, real_t rdeg_y = 0, real_t rdeg_z = 0) :
		fname(fname), picname(picname),
		width(width), height(height),
		ztrans(ztrans), length(length),
		rdeg_x(rdeg_x), rdeg_y(rdeg_y), rdeg_z(rdeg_z) {}
};



/**
 * Homework 0: ray tracing
 */
void hw0(int argc, char *argv[])
{
	/* {{{ hw0 */
	try
	{

		RayTracing tracer;
		tracer.setTraceDepth(15);
		tracer.setMinTraceEnergy(0.05);
		tracer.setThreadNumber(4);

		Scene scene;
		scene.setShadow(true);
		scene.setAntiAliasing(AA_NONE);
		scene.setCFAACoefficient(0.1);
		scene.setEnableMediumDensity(true);

		int enable_kdtree = -1;

		progname = argv[0];
		option long_options[] = {
			{"help",		no_argument,	NULL, 'h'},
			{"shadow", required_argument, NULL, 'l'},
			{"anti-aliasing", required_argument , NULL, 'a'},
			{"medium-density", required_argument, NULL, 'm'},
			{"trace-depth",		required_argument, NULL, 'd'},
			{"nthread", required_argument, NULL, 'n'},
			{"output",		required_argument, NULL, 'o'},
			{"size",		required_argument, NULL, 's'},
			{"show",		no_argument, NULL, 'p'},
			{"kd-tree",		required_argument, NULL, 'k'}
		};

		std::string output_file="output.png";
		std::string image_size;

		bool soft_shadow = false;
		bool show = false;
		int opt;
		while ((opt = getopt_long(argc, argv, "hd:n:m:l:a:o:s:k:", long_options, NULL)) != -1)
		{
			switch (opt)
			{
				case 'h':
					print_usage();
					return 0;
				case 'l':
					{
						int id = atoi(optarg);
						if (id == 0)
							scene.setShadow(false);
						else 
						{
							scene.setShadow(true);
							if (id == 1)
								continue;
							if (id == 2)
								soft_shadow = true;
							else
							{
								printf("ERROR: Unkown shadown mode.\n");
								print_usage();
								return 1;
							}
						}
					}
					break;
				case 'a':
					{
						int id = atoi(optarg);
						if (id == 0)
							scene.setAntiAliasing(AA_NONE);
						else if (id == 1)
							scene.setAntiAliasing(AA_CFAA);
						else if (id == 2)
							scene.setAntiAliasing(AA_FSAA);
						else
						{
							printf("ERROR: invalid Anti-Aliasing mode.\n");
							print_usage();
							return 1;
						}
					}
					break;
				case 'm':
					{
						int id = atoi(optarg);
						if (id == 0)
							scene.setEnableMediumDensity(false);
						else if (id == 1)
							scene.setEnableMediumDensity(true);
						else 
						{
							printf("ERROR: invalid Medium Density mode.\n");
							print_usage();
							return 1;
						}
					}
					break;
				case 'o':
					output_file = optarg;
					break;
				case 's':
					image_size = optarg;
					break;
				case 'd':
					{
						int id = atoi(optarg);
						if (id >= 1)
							tracer.setTraceDepth(id);
						else
						{
							printf("ERROR: invalid Trace Depth.\n");
							print_usage();
							return 1;
						}
					}
					break;
				case 'n':
					{
						int id = atoi(optarg);
						if (id >= 1)
							tracer.setThreadNumber(id);
						else
						{
							printf("ERROR: invalid thread number.\n");
							print_usage();
							return 1;
						}
					}
					break;
				case 'k':
					{
						int id = atoi(optarg);
						if (id >= 0)
							enable_kdtree = (id > 0 ? -1 : 0);
						else
						{
							printf("ERROR: invalid kd-tree parameter.\n");
							print_usage();
							return 1;
						}
					}
					break;
				case 'p':
					show = true;
					break;
				default:
					print_usage();
					return 1;
			}
		}

		TIMING_START();

		tracer.setKdTree(enable_kdtree);


		int width, height;

		if (image_size.length() == 0)
			width = 1920, height = 1080;
		else
		{
			Magick::Image image_tmp(image_size.c_str(), "black");

			width = image_tmp.columns();
			height = image_tmp.rows();
			if (width == 0 || height == 0)
			{
				printf("ERROR: Invalid image size.\n");
				return 1;
			}
		}

		scene.setImageResolution(width, height);

		real_t frustum_half_width = 10.666666666;
		real_t frustum_half_height = frustum_half_width / width * height;

		scene.setFrustum(-frustum_half_width, frustum_half_width, 
				-frustum_half_height, frustum_half_height, 20);

#if 1
		Vector view_point(-20, 0, -60);
		Vector view_pos(0, 4, 20),
			   view_dir = view_point - view_pos,
			   up_dir = -Vector(0, 1, 0).cross(view_dir).cross(view_dir);
#else
		//Vector view_point(0, 0, -100);
		//Vector view_point(0, -7, -43); // Big soccer
		Vector view_point(-7, -4, -28); 
		Vector view_pos(-7, -4, -14);
		//Vector view_pos(0, 0, -7);
		Vector view_dir = view_point - view_pos,
			   up_dir = -Vector(0, 1, 0).cross(view_dir).cross(view_dir);
#endif

		scene.setView(view_pos, view_dir, up_dir);

		scene.setAmbient(0.1, 0.1, 0.1);

		/*** Renderables ***/

		Intensity red(1, 0, 0),
				  green(0, 1, 0),
				  blue(0, 0, 1),
				  white(1, 1, 1),
				  black(0, 0, 0);


		TextureMono tex_blue_0("blue", 
				LightBlendCoefficient(0.5, 0.3, 0.5), 
				LightEnergyCoefficient(0.8, 0.1, 0.1)),
					tex_blue_1(Intensity(0, 0, 1),
							LightBlendCoefficient(0.5, 0.3, 0.5), 
							LightEnergyCoefficient(0.8, 0.5, 0.3)),
					tex_green_0("green", 
							LightBlendCoefficient(0.7, 0.6, 0.4), 
							LightEnergyCoefficient(0.8, 0.3, 0.3)),
					tex_red_0("red", 
							LightBlendCoefficient(0.7, 0.6, 0.4), 
							LightEnergyCoefficient(0.8, 0.3, 0.3)),
					tex_white_0("white",
							LightBlendCoefficient(1, 0.5, 0.6),
							LightEnergyCoefficient(1.0, 0.8, 0.0)),
					tex_mirror("white",
							LightBlendCoefficient(0.1, 0.1, 1.0),
							LightEnergyCoefficient(0.05, 1.0, 0.0)),
					tex_translucent_mirror("white",
							LightBlendCoefficient(1.0, 1.0, 1.0),
							LightEnergyCoefficient(0.2, 1.0, 0.0)),
					tex_translucent_mirror_1("white",
							LightBlendCoefficient(1.0, 1.0, 1.0),
							LightEnergyCoefficient(1.0, 1.0, 0.0)),
					tex_glass("white",
							LightBlendCoefficient(0.1, 0.1, 1.0),
							LightEnergyCoefficient(0.05, 0.0, 1.0)),
					tex_translucent_glass("white",
							LightBlendCoefficient(1.0, 1.0, 1.0),
							LightEnergyCoefficient(0.2, 0.0, 1.0)),
					tex_tg_0("white",
							LightBlendCoefficient(1.0, 1.0, 1.0),
							LightEnergyCoefficient(0.1, 0.2, 1.8)),
					tex_teapot(Intensity(156 / 255.0, 102 / 255.0, 31 / 255.0),
							LightBlendCoefficient(0.7, 1.0, 1.0),
							LightEnergyCoefficient(0.8, 0.8, 0.0)),
					tex_horse(Intensity(156 / 255.0, 102 / 255.0, 31 / 255.0),
							LightBlendCoefficient(0.7, 1.0, 1.0),
							LightEnergyCoefficient(0.8, 0.8, 0.0));

		SurfaceProperty suf_0("white", 1.0, 1.0, 0.2, 0.8, 0.4, 0.0),
						suf_1("black", 1.0, 1.0, 0.2, 0.8, 0.4, 0.0);
		TextureSimpleGrid tex_wbgrid(suf_0, suf_1, 3, 3);


		SurfaceProperty suf_2("white", 1.0, 1.0, 0.2, 0.8, 0.4, 0.0),
						suf_3("gray", 1.0, 1.0, 0.2, 0.8, 0.4, 0.0);
		TextureSimpleGrid tex_wggrid(suf_2, suf_3, 3, 3);



#if 0
		// test teapot

		scene.setImageResolution(1920 / 3, 1080 / 3);
		scene.setFrustum(-10.666666666, 10.6666666666, -6, 6, 20);
		do
		{
			Vector view_point(0, 0, -100);
			Vector view_pos(0, 2, 0),
				   view_dir = view_point - view_pos,
				   up_dir = -Vector(0, 1, 0).cross(view_dir).cross(view_dir);
			scene.setView(view_pos, view_dir, up_dir);
		} while (0);

		scene.addRenderable(new RenderablePrimitive(
					new Plane(0, -7, 0, 0, 1, 0, 1, 0, 1),
					new Material(new TextureSimpleGrid(tex_wbgrid)),
					"plane floor"));

		auto teapot = loadMeshFromObjFile("./res/obj/teapot.obj");
		vector<Vector> pts;
		teapot->normalize();
		teapot->scale(10);

		teapot->rotatedeg(0, 0, -30);
		//teapot->translate(4, 5, 0);

		//teapot->translate(-5, -4, -30);
		//teapot->translate(-16, -2, -30);
		teapot->translate(0, 0, -35);
		teapot->setNoInside();
		teapot->setNormalInterpolation(false);

		teapot->getVertexVector(pts);
		teapot->finished();
		BDObjectSphere bdsphere(pts);

		scene.addRenderable(new RenderablePrimitive(
					new Mesh(*teapot),
					new Material(new TextureMono(tex_teapot)),
					"teapot", new BDObjectSphere(bdsphere)));
#endif



		// whole
#if 1

#if 1
		fprintf(stderr, "Loading textures ...\n");
		TextureBitmap tex_bitmap("./res/texture/WaterColour.orig.jpg",
				1.0, 1.0,
				LightBlendCoefficient(1.0, 1.0, 1.0),
				LightEnergyCoefficient(1.0, 0.0, 0.0));

		TextureBitmap tex_wall("./res/texture/wall.orig.jpg",
				86.02150537634408, 60, 
				LightBlendCoefficient(1.0, 1.0, 0.1),
				LightEnergyCoefficient(1.0, 0.0, 0.0));

		TextureBitmap tex_soccer("./res/texture/soccer.big.jpg",
				1.0, 1.0 ,
				LightBlendCoefficient(1.0, 1.0, 0.1),
				LightEnergyCoefficient(1.0, 0.0, 0.0));

		fprintf(stderr, "End loading textures ...\n");
#endif

		fprintf(stderr, "Loading models ...\n");
#if 1
		// buddha
		auto buddha = loadMeshFromObjFile("./res/obj/test_data/Buddha.obj");

		do
		{
			vector<Vector> pts;
			buddha->normalize();
			buddha->scale(4.8);

			buddha->translate(0.5, 0, -17);

			buddha->setNoInside();
			buddha->setNormalInterpolation(true);

			buddha->getVertexVector(pts);
			buddha->setKdTree(enable_kdtree);

			buddha->finish();

			//printMeshToObjFile(buddha.ptr(), "buddha.rotate.obj");

			BDObjectSphere bdsphere(pts);
			BDObjectAABox bdaabox(pts);

			scene.addRenderable(new RenderablePrimitive(
						new Mesh(*buddha),
						new Material(new TextureMono(tex_horse)),
						//"buddha", new BDObjectSphere(bdsphere)));
				"buddha", new BDObjectAABox(bdaabox)));
		} while (0);
#endif

#if 1
		// dragon
		auto dragon = loadMeshFromObjFile("./res/obj/test_data/fixed.perfect.dragon.100K.0.07.obj");

		do
		{
			vector<Vector> pts;
			dragon->normalize();
			dragon->rotatedeg(0, -135, 0);
			dragon->scale(6);
			dragon->translate(-20, 0, -17);

			dragon->setNoInside();
			dragon->setNormalInterpolation(true);

			dragon->getVertexVector(pts);
			dragon->setKdTree(enable_kdtree);

			dragon->finish();

			//printMeshToObjFile(dragon.ptr(), "dragon.rotate.obj");

			BDObjectSphere bdsphere(pts);
			BDObjectAABox bdaabox(pts);

			scene.addRenderable(new RenderablePrimitive(
						new Mesh(*dragon),
						new Material(new TextureMono(tex_teapot)),
						//"dragon", new BDObjectSphere(bdsphere)));
				"dragon", new BDObjectAABox(bdaabox)));
		} while (0);
#endif


#if 1

		scene.addRenderable(new RenderablePrimitive(
					new Sphere(8, 5, -70, 7),
					new Material(new TextureBitmap(tex_bitmap)),
					"bitmap sphere"));

		/*
		   scene.addRenderable(new RenderablePrimitive(
		   new Sphere(0, 1, -100, 5),
		   new Material(new TextureMono(tex_blue_0)),
		   "blue sp"));
		   */

		scene.addRenderable(new RenderablePrimitive(
					new Sphere(-15, -2, -60, 5),
					new Material(new TextureBitmap(tex_soccer)),
					"soccer sp"));

		scene.addRenderable(new RenderablePrimitive(
					new Sphere(-12, -1, -85, 5),
					new Material(new TextureMono(tex_mirror)),
					"mirror sp"));

		scene.addRenderable(new RenderablePrimitive(
					new Sphere(2, 1.1, -30, 4),
					new Material(new TextureMono(tex_glass),
						5, 1.333, 0.0),
					"glass sp 1"));

#endif
		// plane

		scene.addRenderable(new RenderablePrimitive(
					new Plane(Vector(-30, 0, 0), Vector(1, -sqrt(3.0), 0)),
					new Material(new TextureMono(tex_translucent_mirror_1)),
					"plane left mirror"));


		scene.addRenderable(new RenderablePrimitive(
					new Plane(0, -7, 0, 0, 1, 0, 1, 0, 1),
					new Material(new TextureSimpleGrid(tex_wbgrid)),
					"plane floor"));

		scene.addRenderable(new RenderablePrimitive(
					new Plane(Vector(0, 0, -150), Vector(0, 0, 1),
						Vector(1, 0, 0)),
					new Material(new TextureBitmap(tex_wall)),
					"plane back wall"));

		/*
		   scene.addRenderable(new RenderablePrimitive(
		   new Plane(50, 0, 0, 1, 0, 0),
		   new Material(new TextureMono(tex_translucent_mirror)),
		   "plane right mirror"));
		   */
		scene.addRenderable(new RenderablePrimitive(
					new Plane(0, 0, 150, 0, 0, 1),
					new Material(new TextureMono(tex_translucent_mirror)),
					"plane behind mirror"));

#if 1
		// mesh
		// tetrahedron
		Mesh mesh_tet;
		real_t tetrahedron_length = 2;
		real_t s3 = sqrt(3.0);
		mesh_tet.addVertex(Vector(0, 0, 2) * tetrahedron_length);
		mesh_tet.addVertex(Vector(s3, 0, -1) * tetrahedron_length);
		mesh_tet.addVertex(Vector(-s3, 0, -1) * tetrahedron_length);
		mesh_tet.addVertex(Vector(0, s3, 0) * tetrahedron_length);

		mesh_tet.addFace(0, 1, 3);
		mesh_tet.addFace(1, 2, 3);
		mesh_tet.addFace(2, 0, 3);
		mesh_tet.addFace(2, 1, 0);

		mesh_tet.translate(1, -2, -40);

		mesh_tet.finish();

		scene.addRenderable(new RenderablePrimitive(
					new Mesh(mesh_tet),
					new Material(new TextureMono(tex_translucent_glass), 5, 1.333, 0.0),
					"tetrahedron"));

#endif

#if 1
		// cuboid
		AutoPtr<Cuboid> cuboid0(new Cuboid(4, 5, 3));
		cuboid0->translate(-25, -4, -30);
		cuboid0->finish();

		scene.addRenderable(new RenderablePrimitive(
					new Cuboid(*cuboid0),
					new Material(new TextureMono(tex_blue_0),
						5, 1.333, 0.0),
					"cuboid0 blue"));

#endif

#if 1
		AutoPtr<Cuboid> cuboid1(new Cuboid(5, 5, 5));
		cuboid1->translate(-7, -4, -28);
		//cuboid1->translate(-16, -2, -30);
		//cuboid1->setNoInside();
		cuboid1->finish();

		scene.addRenderable(new RenderablePrimitive(
					new Cuboid(*cuboid1),
					new Material(new TextureMono(tex_tg_0),
						5, 1.3333, 0.0),
					"cuboid1 glass"));
#endif

		// obj file

#if 0
		// humanoid_tri: normal vector cruption
		AutoPtr<Mesh> mesh = loadMeshFromObjFile("./res/obj/humanoid_tri.obj");
		mesh->normalize();
		real_t size = 4;
		mesh->scale(size);
		mesh->rotatedeg(180, 90, -90);
		mesh->rotatedeg(0, 45, 0);
		mesh->translate(0, -4 + -size * 0.5, -30);
		mesh->setNoInside();
		mesh->finish();

		scene.addRenderable(new RenderablePrimitive(
					new Mesh(*mesh),
					new Material(new TextureMono(tex_blue_0)),
					"humanoid_tri"));
#endif



#if 1
		// teapot
		auto teapot = loadMeshFromObjFile("./res/obj/teapot.obj");
		vector<Vector> pts;
		teapot->normalize();
		teapot->scale(7);

		teapot->rotatedeg(0, 0, -30);
		teapot->translate(0, 3, 0);

		//teapot->translate(-5, -4, -30);
		teapot->translate(-16, -2, -30);
		//teapot->translate(0, 0, -30);
		teapot->setNoInside();
		teapot->setNormalInterpolation(true);

		teapot->setKdTree(enable_kdtree);
		teapot->getVertexVector(pts);

		teapot->finish();
		BDObjectSphere bdsphere(pts);

		scene.addRenderable(new RenderablePrimitive(
					new Mesh(*teapot),
					new Material(new TextureMono(tex_teapot)),
					"teapot", new BDObjectSphere(bdsphere)));
#endif


#endif



		// KdTree Test
#if 0

		do
		{
			Vector view_point(0, 0, -40);
			Vector view_pos(0, 0, 0),
				   view_dir = view_point - view_pos,
				   up_dir = -Vector(0, 1, 0).cross(view_dir).cross(view_dir);

			scene.setView(view_pos, view_dir, up_dir);
		} while (0);


#if 0
		// cuboid
		AutoPtr<Cuboid> cuboid0(new Cuboid(4, 5, 3));
		cuboid0->setKdTree(true);
		cuboid0->scale(2, 2, 2);
		cuboid0->rotate(0, 0, 30);
		cuboid0->setNoInside();
		cuboid0->translate(0, -4, -40);
		cuboid0->finish();

		scene.addRenderable(new RenderablePrimitive(
					new Cuboid(*cuboid0),
					new Material(new TextureMono(tex_blue_0),
						5, 1.333, 0.0),
					"cuboid0 blue"));

		scene.addRenderable(new RenderablePrimitive(
					new Plane(0, -7, 0, 0, 1, 0, 1, 0, 1),
					new Material(new TextureSimpleGrid(tex_wbgrid)),
					"plane floor"));
#endif


#if 0
		// kitten 
		auto kitten = loadMeshFromObjFile("./res/obj/test_data/kitten.50k.obj");

		do
		{
			vector<Vector> pts;
			kitten->normalize();
			kitten->scale(15);

			kitten->translate(0, 0, -60);

			kitten->setNoInside();
			kitten->setNormalInterpolation(true);

			kitten->getVertexVector(pts);
			kitten->setKdTree(enable_kdtree);

			kitten->finish();

			//printMeshToObjFile(kitten.ptr(), "kitten.rotate.obj");

			BDObjectSphere bdsphere(pts);
			BDObjectAABox bdaabox(pts);

			scene.addRenderable(new RenderablePrimitive(
						new Mesh(*kitten),
						new Material(new TextureMono(tex_horse)),
						//"kitten", new BDObjectSphere(bdsphere)));
				"kitten", new BDObjectAABox(bdaabox)));
		} while (0);
#endif

#if 0
		// buddha
		auto buddha = loadMeshFromObjFile("./res/obj/test_data/Buddha.obj");

		do
		{
			vector<Vector> pts;
			buddha->normalize();
			buddha->scale(15);

			buddha->translate(0, 0, -60);

			buddha->setNoInside();
			buddha->setNormalInterpolation(false);

			buddha->getVertexVector(pts);
			buddha->setKdTree(enable_kdtree);

			buddha->finish();

			//printMeshToObjFile(buddha.ptr(), "buddha.rotate.obj");

			BDObjectSphere bdsphere(pts);
			BDObjectAABox bdaabox(pts);

			scene.addRenderable(new RenderablePrimitive(
						new Mesh(*buddha),
						new Material(new TextureMono(tex_horse)),
						//"buddha", new BDObjectSphere(bdsphere)));
				"buddha", new BDObjectAABox(bdaabox)));
		} while (0);
#endif

#if 1
		// dragon
		auto dragon = loadMeshFromObjFile("./res/obj/test_data/fixed.perfect.dragon.100K.0.07.obj");

		do
		{
			vector<Vector> pts;
			dragon->normalize();
			dragon->scale(300);

			dragon->translate(0, 0, -40);

			dragon->setNoInside();
			dragon->setNormalInterpolation(false);

			dragon->getVertexVector(pts);
			dragon->setKdTree(enable_kdtree);

			dragon->finish();

			//printMeshToObjFile(dragon.ptr(), "dragon.rotate.obj");

			BDObjectSphere bdsphere(pts);
			BDObjectAABox bdaabox(pts);

			scene.addRenderable(new RenderablePrimitive(
						new Mesh(*dragon),
						new Material(new TextureMono(tex_horse)),
						//"dragon", new BDObjectSphere(bdsphere)));
				"dragon", new BDObjectAABox(bdaabox)));
		} while (0);
#endif


#if 0
		// horse
		auto horse = loadMeshFromObjFile("./res/obj/test_data/horse.fine.90k.obj");

		do
		{
			vector<Vector> pts;
			horse->normalize();
			horse->scale(15);
			horse->rotatedeg(0, 90, 0);
			horse->rotatedeg(0, 0, 90);


			horse->translate(0, 0, -40);

			horse->setNoInside();
			horse->setNormalInterpolation(false);

			horse->getVertexVector(pts);
			horse->setKdTree(enable_kdtree);

			horse->finish();

			//printMeshToObjFile(horse.ptr(), "horse.rotate.obj");

			BDObjectSphere bdsphere(pts);
			BDObjectAABox bdaabox(pts);

			scene.addRenderable(new RenderablePrimitive(
						new Mesh(*horse),
						new Material(new TextureMono(tex_horse)),
						//"horse", new BDObjectSphere(bdsphere)));
				"horse", new BDObjectAABox(bdaabox)));
		} while (0);

#endif

#if 0

		int ntp = 3, mtp = 2;
		AutoPtr<Mesh> tp[ntp * mtp];
		int nn = 0;
		double len = 15, factor = 1.3;
		for (int i = 0; i < ntp; i ++)
			for (int j = 0; j < mtp; j ++)
			{
				// teapot
				auto teapot = tp[nn ++] = loadMeshFromObjFile("./res/obj/teapot.obj");
				vector<Vector> pts;
				teapot->normalize();
				teapot->scale(len);

				teapot->translate((i - ntp / 2) * len * factor, (j - mtp / 2) * len * factor, -80);

				teapot->setNoInside();
				teapot->setNormalInterpolation(true);

				teapot->getVertexVector(pts);
				teapot->setKdTree(enable_kdtree);

				teapot->finish();
				BDObjectSphere bdsphere(pts);

				scene.addRenderable(new RenderablePrimitive(
							new Mesh(*teapot),
							new Material(new TextureMono(tex_teapot)),
							"teapot", new BDObjectSphere(bdsphere)));

			}
#endif

		scene.setAmbient(0.1, 0.1, 0.1);

		// add sphere
#if 0
		real_t sphere_rad = 3;
		int n = 320, m = 320;
		real_t dist = 200;
		AutoPtr<Material> blue_mat = new Material(new TextureMono(tex_blue_0));
		for (int i = 0; i < n; i ++)
			for (int j = 0; j < m; j ++)
			{
				scene.addRenderable(new RenderablePrimitive(
							new Sphere((i - n / 2) * (sphere_rad * 2.1),
								(j - m / 2) * (sphere_rad * 2.1), -dist, sphere_rad),
							blue_mat,
							"blue sp"));

			}
#endif

#endif



#if 1
		// LIGHT
		if (soft_shadow)
		{
			real_t cx = 40, cy = 40;
			int A = 5, B = 5;
			real_t light = 1.8 / (real_t)(A * B);
			real_t length = 20;

			for (int i = 0; i < A; i ++)
				for (int j = 0; j < B; j ++)
				{
					real_t x, y;
					if (A == 1) x = cx;
					else x = cx - length * 0.5 + i / (real_t)(A - 1) * length;
					if (B == 1) y = cy;
					else y = cy - length * 0.5 + j / (real_t)(B - 1) * length;
					scene.addLightSource(x, y, 0, light);
				}
		}
		else
		{
			scene.addLightSource(30, 0, -30, 0.1);
			scene.addLightSource(40, 40, 0, 0.9);
		}
#endif

		TIMING_END("initialize");

		// lets roll!
		TIME(tracer.render(scene));

		scene.write(output_file.c_str());
		printf("output file has been written to `%s`\n", output_file.c_str());

		if (show)
			scene.display();
	}
	catch (CGException &exc)
	{
		printf("CGException caught. terminating.\n");
	}

	/* }}} */
}


/**
 * Homework 1: mesh simplification
 */
void hw1(int argc, char *argv[])
{
	/* {{{ hw1 */
	try
	{
		std::string output_dir = "mesh-simp";
		vector<real_t> ratio_seq = {0, 0.25, 0.5, 0.75, 0.90, 0.95, 0.98};
		bool show = true;

		int enable_kdtree = -1;
		int trace_depth = 1;
		int nthread = 4;
		int enble_shadow = 0;

		RayTracing tracer;
		tracer.setTraceDepth(trace_depth);
		tracer.setMinTraceEnergy(0.05);
		tracer.setThreadNumber(nthread);

		tracer.setKdTree(enable_kdtree);

		Scene scene;
		scene.setShadow(enble_shadow);
		scene.setEnableMediumDensity(true);

		Vector view_point(0, 0, -100);
		Vector view_pos(0, 0, 0),
			   view_dir = view_point - view_pos,
			   up_dir = -Vector(0, 1, 0).cross(view_dir).cross(view_dir);

		scene.setView(view_pos, view_dir, up_dir);

		// lights
		scene.setAmbient(0.1, 0.1, 0.1);
		scene.addLightSource(40, 40, 0, 0.9);


		/* {{{ mesh */

		MeshConf meshconf[] = {
			MeshConf("./res/obj/test_data/cube.obj", "cube", 1280, 1280, -40, 14, 45, 60, 45),
			MeshConf("./res/obj/test_data/Arma.obj", "Arma", 1280, 1280, -36, 16, 0, 150),
			MeshConf("./res/obj/test_data/block.obj", "block", 1280, 1280, -40, 17, 45, 45, 45),
			MeshConf("./res/obj/test_data/Buddha.obj", "Buddha", 800, 1280, -35, 25),
			MeshConf("./res/obj/test_data/bunny.fine.obj", "bunny", 1280, 800, -40, 14, 0, 15),
			MeshConf("./res/obj/test_data/dinosaur.2k.obj", "dinosaur", 1000, 1280, -40, 18, -90, -90),
			MeshConf("./res/obj/test_data/fandisk.18k.obj", "fandisk", 1280, 1280, -40, 20, 120, 45, 45),
			MeshConf("./res/obj/test_data/fixed.perfect.dragon.100K.0.07.obj", "dragon", 1280, 800, -40, 14, 15),
			MeshConf("./res/obj/test_data/horse.fine.90k.obj", "horse", 1280, 800, -43, 14, -90, 90, 0),
			MeshConf("./res/obj/test_data/kitten.50k.obj", "kitten", 1000, 1280, -40, 20),
			MeshConf("./res/obj/test_data/rocker-arm.18k.obj", "rocker", 1280, 800, -40, 14, -45, 75, 120),
			MeshConf("./res/obj/test_data/sphere.obj", "sphere", 1280, 1280, -40, 17),
		};

		real_t smaller = 1;
		for (auto conf: meshconf)
		{

			conf.width *= smaller;
			conf.height *= smaller;
			scene.setImageResolution(conf.width, conf.height);

			real_t frustum_half_width = 10.666666666;
			real_t frustum_half_height = frustum_half_width / conf.width * conf.height;

			scene.setFrustum(-frustum_half_width, frustum_half_width, 
					-frustum_half_height, frustum_half_height, 20);

			auto mesh = loadMeshFromObjFile(conf.fname); 
			mesh->setKdTree(false);

			mesh->translateToOrigin();
			mesh->rotatedeg(conf.rdeg_x, 0, 0);
			mesh->rotatedeg(0, conf.rdeg_y, 0);
			mesh->rotatedeg(0, 0, conf.rdeg_z);
			mesh->translateToOrigin();

			mesh->normalize();

			mesh->scale(conf.length);


			mesh->translate(0, 0, conf.ztrans);
			mesh->finish();

			TIMING_START();
			auto meshes = mesh->simplify(ratio_seq);
			assert(meshes.size() == ratio_seq.size());

			TIMING_END("mesh simplify");

			TextureMono tex_mesh(Intensity(156 / 255.0, 102 / 255.0, 31 / 255.0),
					LightBlendCoefficient(0.7, 1.0, 1.0),
					LightEnergyCoefficient(0.8, 0.8, 0.0));


			int cnt = 0;
			for (size_t ord = 0; ord < meshes.size(); ord ++)
			{
				fprintf(stderr, "current simplification ratio: %.2lf\n", ratio_seq[ord]);
				auto smesh = meshes[ord];

				smesh->setNormalInterpolation(false);
				smesh->setNoInside();
				smesh->setKdTree(enable_kdtree);

				smesh->finish();

				vector<Vector> pts;
				smesh->getVertexVector(pts);

				scene.clearRenderable();
				scene.addRenderable(new RenderablePrimitive(
							new Mesh(*smesh),
							new Material(new TextureMono(tex_mesh)),
							conf.picname, new BDObjectAABox(pts))); 

				TIME(tracer.render(scene));

				string output_file = output_dir + "/" + conf.picname + ssprintf("-%.2lf", ratio_seq[ord]) + ".png";
				auto image = scene.getImageRenderer();

				std::string text = 
					ssprintf("%s: ratio=%.2lf, nvtx=%d, nface=%d", conf.picname.c_str(),
							ratio_seq[ord], smesh->nvtx(), smesh->nface());

				double fps = conf.width * 0.3 / 10;
				double font_point_size = std::min(40.0, std::max(10.0, fps));

				image->drawText(font_point_size / 2, font_point_size, text, "white", font_point_size, 1);

				scene.write(output_file.c_str());
				printf("output file has been written to `%s`\n", output_file.c_str());

				cnt ++;

				if (meshes.size() == 1)
					scene.display();
			}
		}
		/* }}} */
	}
	catch (CGException &exc)
	{
		printf("CGException caught. terminating.\n");
	}
	catch (const char *msg)
	{
		printf("Exception caught: %s\n", msg);
	}

	/* }}} */
}

int main(int argc, char *argv[])
{
	if (!strcmp(argv[0], "ray-tracing"))
		hw0(argc, argv);
	else hw1(argc, argv);
	return 0;
}

/**
 * vim: fdm=marker
 */

