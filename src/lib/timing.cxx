/*
 * $File: timing.cxx
 * $Date: Mon Apr 30 14:36:53 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "timing.hxx"

std::stack<std::pair<clock_t, timespec>> __Timing_namespace::time_stack;
