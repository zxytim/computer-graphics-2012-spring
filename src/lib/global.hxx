/*
 * $File: global.hxx
 * $Date: Fri Jun 08 00:13:39 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __LIB_COMMON__
#define __LIB_COMMON__

typedef double real_t;

const real_t EPS = 1e-6;

const real_t REAL_INFINITY = 1e30;

#endif // __LIB_COMMON__
