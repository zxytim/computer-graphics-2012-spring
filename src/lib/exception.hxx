/*
 * $File: exception.hxx
 * $Date: Tue Apr 24 20:38:18 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __LIB_EXCEPTION__
#define __LIB_EXCEPTION__

#include <cstdarg>
#include <string>

using std::string;

class CGException 
{
	protected:
		static const int EXCEPTION_MSG_LENGTH_MAX = 10000;
		string m_msg;
		virtual const char *prompt() const throw ();
		static char msg_buf[EXCEPTION_MSG_LENGTH_MAX];
	public:
		CGException();
		CGException(const char *fmt, ...);
		virtual string msg() const;
		virtual const char *what() const throw();

		virtual ~CGException();
};

#define CGEXCEPTION_DEFINE_CONSTRUCTOR_IN_CLASS(class_name) \
	class_name(const char *fmt, ...) \
	{ \
		va_list args; \
		va_start(args, fmt); \
		vsnprintf(msg_buf, EXCEPTION_MSG_LENGTH_MAX - 1, fmt, args); \
		va_end(args); \
		m_msg = prompt(); \
		m_msg += msg_buf; \
	} 

#endif // __LIB_EXCEPTION__
