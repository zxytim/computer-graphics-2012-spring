/*
 * $File: auto_ptr.hxx
 * $Date: Mon Jun 18 00:37:08 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __LIB_AUTO_PTR__
#define __LIB_AUTO_PTR__

//#include <cstddef>
#include "exception.hxx"
#include "sassert.hxx"
#include <cstring>

class CGExceptionAutoPtr : public CGException
{
	public:
		const char *what() 
		{ return "CG exception AutoPtr"; }
		const char *prompt()
		{ return "[AutoPtrExc]"; }
		CGEXCEPTION_DEFINE_CONSTRUCTOR_IN_CLASS(CGExceptionAutoPtr);

};

// refer to `C++ Primer`
template<typename T>
class AutoPtr
{
	private:
		// the actual pointer
		T *m_ptr;

		// number of used reference
		size_t * m_use; 

		void remove_ref()
		{
			if (-- *m_use == 0)
			{
				delete m_ptr;
				delete m_use;
#ifdef __DEBUG_BUILD
				m_ptr = NULL;
				m_use = NULL;
#endif
			}
		}

	public:
		AutoPtr(T *ptr = nullptr)
			: m_ptr(ptr), m_use(new size_t(1))
		{
		}

		~AutoPtr()
		{
			remove_ref();
		}

		AutoPtr(const AutoPtr<T> &auto_ptr)
			: m_ptr(auto_ptr.m_ptr), m_use(auto_ptr.m_use)
		{
			++ *m_use;
		}

		AutoPtr<T> & operator = (const AutoPtr<T> &ptr)
		{
			++ *ptr.m_use; // protect against self-assignment
			remove_ref(); // decrement use count and delete pointers if needed

			m_ptr = ptr.m_ptr;
			m_use = ptr.m_use;

			return *this;
		}

		inline T& operator * ()
		{
			sassert(m_ptr);
			return *m_ptr;
			throw CGExceptionAutoPtr("dereference of null pointer");
		}

		inline const T& operator *() const
		{
			sassert(m_ptr);
			return *m_ptr;
			throw CGExceptionAutoPtr("dereference of null pointer");
		}


		inline T* operator -> ()
		{
			sassert(m_ptr);
			return m_ptr;
			throw CGExceptionAutoPtr("access of null pointer");
		}


		inline const T* operator -> () const
		{
			sassert(m_ptr);
			return m_ptr;
			throw CGExceptionAutoPtr("access of null pointer");
		}

		inline bool operator == (const T * ptr) const
		{
			return m_ptr == ptr;
		}

		inline T * ptr()
		{
			return m_ptr;
		}

		inline T * ptr() const
		{
			return m_ptr;
		}

		operator bool() const
		{ return m_ptr != nullptr; }

#if 0
		inline AutoPtr copy() const
		{
			AutoPtr ret;
			ret.m_ptr = (decltype(ret.m_ptr)) malloc(sizeof(T));
			memcpy(ret.m_ptr, m_ptr, sizeof(T));
			*ret.m_use = 1;
			return ret;
		}
#endif
};


#endif // __LIB_AUTO_PTR__

/**
 * vim: ft=cpp
 */
