/*
 * $File: obj.cxx
 * $Date: Tue Jun 19 13:37:04 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "obj.hxx"
#include <sassert.hxx>

#include <cstdio>
#include <cstring>

#include <string>

namespace ObjParser
{
	void read(string fname, vector<Vertex> &vtx, 
			vector<Face> &face)
	{
		FILE *fin = fopen(fname.c_str(), "r");
#ifdef __DEBUG_BUILD
		assert(fin);
#else
		if (!fin)
			throw ((std::string)"Can not open file " + fname).c_str();
#endif
		static const int LINE_LENGTH_MAX = 10000;
		static char str[LINE_LENGTH_MAX + 1];

		vtx.clear(), face.clear();
		int nnorms = 0;
		while (fgets(str, LINE_LENGTH_MAX, fin))
		{
			if (str[0] == '#' || strlen(str) == 0)
				continue;
			if (str[0] == 'v')
			{
				if (str[1] == ' ') // v
				{
					Vertex v;
					double x, y, z;
					sscanf(str + 2, "%lf%lf%lf", &x, &y, &z);
					v.x = x, v.y = y, v.z = z;
					v.has_normal = false;
					vtx.push_back(v);
					continue;
				}
				else if (str[1] == 'n') // vn
				{
					double x, y, z;
					sscanf(str + 2, "%lf%lf%lf", &x, &y, &z);
					nnorms ++;
					sassert(nnorms <= (int)vtx.size());
					Vertex &v = vtx[nnorms - 1];
					v.n_x = x, v.n_y = y, v.n_z = z;
					v.has_normal = true;
				}
			}

			if (str[0] == 'f')
			{
				static char buf[3][100];
				sscanf(str + 2, "%s%s%s", buf[0], buf[1], buf[2]);
				int a, b, c;
				sscanf(buf[0], "%d", &a);
				sscanf(buf[1], "%d", &b);
				sscanf(buf[2], "%d", &c);

				Face f;
				f.v0 = a - 1, f.v1 = b - 1, f.v2 = c - 1;

				face.push_back(f);
			}
		}

		fclose(fin);
	}
}
