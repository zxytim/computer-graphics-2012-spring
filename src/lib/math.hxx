/*
 * $File: math.hxx
 * $Date: Thu Jun 07 23:57:27 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __HEADER_MATH__
#define __HEADER_MATH__

#include <cmath>
#include "global.hxx"


#define sqr __sqr


long double __sqr(long double x);
double __sqr(double x);
float __sqr(float x);
int __sqr(int x);

// XXX
#if 0
// refer to: http://www.beyond3d.com/content/articles/8/
inline float InvSqrtf (float x)
{
	float xhalf = 0.5f*x;
	int i = *(int*)&x;
	i = 0x5f3759df - (i>>1);
	x = *(float*)&i;
	x = x*(1.5f - xhalf*x*x);
	return x;
}

// refer to: http://www.gamedev.net/topic/500229-optimized-invsqrt-double-/
inline double InvSqrtd( const double& x )
{
	double y = x;
	double xhalf = ( double )0.5 * y;
	long long i = *( long long* )( &y );
	i = 0x5fe6ec85e7de30daLL - ( i >> 1 );//LL suffix for (long long) type for GCC
	y = *( double* )( &i );
	y = y * ( ( double )1.5 - xhalf * y * y );

	return y;
}

#endif
#endif // __HEADER_MATH__
