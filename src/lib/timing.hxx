/*
 * $File: timing.hxx
 * $Date: Mon Apr 30 14:37:03 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __LIB_TIMING__
#define __LIB_TIMING__

#include <stack>
#include <ctime>
#include <algorithm>

class __Timing_namespace
{
	public:
		static std::stack<std::pair<clock_t, timespec>> time_stack;
};

#define TIME(expr) \
	({ \
		clock_t cstart = clock(); \
		timespec start, end; \
		clock_gettime(CLOCK_REALTIME, &start); \
		(expr); \
		clock_t cend = clock(); \
		clock_gettime(CLOCK_REALTIME, &end); \
		double duration = ((end.tv_sec - start.tv_sec) * 1000000000.0 + \
						(end.tv_nsec - start.tv_nsec)) / 1000000.0; \
		fprintf(stderr, "%s: cputime = %.0lfms, real = %.0lfms\n", #expr, (cend - cstart) / (double)CLOCKS_PER_SEC * 1000, duration); \
	 })

#define TIMING_START() \
	({ \
		clock_t cstart = clock(); \
		timespec start; \
		clock_gettime(CLOCK_REALTIME, &start); \
		__Timing_namespace::time_stack.push(std::make_pair(cstart, start)); \
	 })

#define TIMING_END(msg) \
	({ \
		timespec end, start = __Timing_namespace::time_stack.top().second; \
		clock_gettime(CLOCK_REALTIME, &end); \
		double duration = ((end.tv_sec - start.tv_sec) * 1000000000.0 + \
						(end.tv_nsec - start.tv_nsec)) / 1000000.0; \
		fprintf(stderr, "%s: cputime = %.0lfms, real = %.0lfms\n", \
			msg, (clock() - __Timing_namespace::time_stack.top().first) / (double)CLOCKS_PER_SEC * 1000, \
			duration); \
		 __Timing_namespace::time_stack.pop(); \
	 })

#endif // __LIB_TIMING__
