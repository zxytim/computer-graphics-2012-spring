/*
 * $File: image.hxx
 * $Date: Wed Jun 20 15:01:56 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __HEADER_IMAGE__
#define __HEADER_IMAGE__

#include <cassert>
#include <sstream>
#include <string>
#include <cstdlib>
#include "color.hxx"

using Magick::Image;
using Magick::Geometry;
using Magick::Pixels;
using Magick::PixelPacket;
using Magick::DrawableText;

/**
 * a wrapper for Magick::Image
 */
class ImageRenderer
{
	protected:

		Image *image;

		static std::string itoa(int num)
		{
			std::stringstream ss;
			ss << num;
			std::string s;
			ss >> s;
			return s;
		}
	public:

		ImageRenderer() {}
		ImageRenderer(int width, int heigth) :
			image(new Image(Geometry(width, heigth), Color("black"))) {}

		ImageRenderer(const ImageRenderer &img) = delete;

		ImageRenderer &operator = (const ImageRenderer &img) = delete;

		~ImageRenderer()
		{
			delete image;
		}

		// for wider use
		ImageRenderer(const char *fname)
			: image(new Image(fname)) {}

		int width() const { return image->columns(); }
		int height() const { return image->rows(); }

		void resize(int width, int height)
		{
			if (image)
				delete image;

			image = new Image(Geometry(width, height), Color("black"));
		}
		Color pixelColor(int x, int y) const
		{ return image->pixelColor(x, height() - y - 1); }
		void pixelColor(int x, int y, const Color &color)
		{ image->pixelColor(x, height() - y - 1, color); }

		void load(const Color *pixels, int width, int height)
		{
			resize(width, height);
			Pixels cache(*image);
			PixelPacket* pixel = cache.get(0, 0, width, height);
			for (int i = 0, cnt = 0; i < width; i ++)
				for (int j = 0; j < height; j ++)
					*(pixel + i * height + (height - j - 1)) = pixels[cnt ++];
			cache.sync();
		}

		void write(const char *fname)
		{
			image->magick("png");
			image->write(fname);
		}


		void display() const
		{
			Image(*image).display();
		}

		void drawText(real_t x, real_t y, const std::string &content,
				const Color &color, real_t font_point_size, int stroke_width)
		{
			image->font("DejaVuSans.ttf");
			image->strokeColor(color); 
			image->strokeWidth(stroke_width);
			image->fontPointsize(font_point_size);
		//	image->draw(Magick::DrawableFont("400", Magick::StyleType::NormalStyle, 0, Magick::StretchType::AnyStretch));

			image->draw(DrawableText(x, y, content));
		}
};

#endif // __HEADER_IMAGE__
