/*
 * $File: exception.cxx
 * $Date: Fri Apr 20 13:45:58 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "exception.hxx"


CGException::CGException()
{
	m_msg = what();
}

CGException::~CGException()
{
}

const char *CGException::prompt() const throw ()
{
	return "[CGException]";
}

CGException::CGException(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vsnprintf(msg_buf, EXCEPTION_MSG_LENGTH_MAX - 1, fmt, args);
	va_end(args);

	m_msg = prompt();
   	m_msg += msg_buf;
}

string CGException::msg() const
{ return m_msg; }


const char *CGException::what() const throw()
{
	return "CG exception happened"; 
}

char CGException::msg_buf[EXCEPTION_MSG_LENGTH_MAX];

