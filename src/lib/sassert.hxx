/*
 * $File: sassert.hxx
 * $Date: Fri Jun 08 19:31:23 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */


#ifndef __HEADER_SASSERT__
#define __HEADER_SASSERT__

#include <cassert>

#ifdef __DEBUG_BUILD
#define sassert(expr) assert((expr))
#else
#define sassert(expr) {}
#endif


#endif // __HEADER_SASSERT__
