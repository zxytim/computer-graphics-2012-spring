/*
 * $File: obj.hxx
 * $Date: Tue Jun 19 13:33:54 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

/**
 * Simple Parser for OBJ files
 */
#ifndef __LIB_OBJ__
#define __LIB_OBJ__

#include "global.hxx"
#include <vector>
#include <string>

namespace ObjParser
{
	using std::vector;
	using std::string;

	struct Vertex
	{
		real_t x, y, z;
		real_t n_x, n_y, n_z;
		bool has_normal;
	};
	struct Point
	{
		real_t x, y, z;
	};
	struct Face
	{ int v0, v1, v2; };
	void read(string fname, vector<Vertex> &vtx, 
			vector<Face> &face);
}

#endif // __LIB_OBJ__
