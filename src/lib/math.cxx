/*
 * $File: math.cxx
 * $Date: Thu Jun 07 23:57:35 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "math.hxx"

long double __sqr(long double x) { return x * x; }
double __sqr(double x) { return x * x; }
float __sqr(float x) { return x * x; }
int __sqr(int x) { return x * x; }
