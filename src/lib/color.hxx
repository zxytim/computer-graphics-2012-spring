/*
 * $File: color.hxx
 * $Date: Wed Apr 25 19:55:46 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __LIB_COLOR__
#define __LIB_COLOR__


#include <Magick++.h>
#include "global.hxx"

// XXX:
// Fix for deprecated macro `Intensity`
// defined in Magick++

#ifdef Intensity
#undef Intensity
#endif

class Color : public Magick::Color
{
	public:
		Color() {}
		Color(const Magick::Color &color)
			:Magick::Color(color)
		{}

		// range in [0 .. 1]
		Color(real_t red, real_t green, real_t blue)
			: Magick::Color(red * MaxRGB, green * MaxRGB, blue * MaxRGB)
		{}
		
		Color(real_t red, real_t green, real_t blue, real_t alpha)
			: Magick::Color(red * MaxRGB, green * MaxRGB, blue * MaxRGB,
					alpha * MaxRGB)
		{}

		Color(const char * color)
			: Magick::Color(color)
		{}
};

#endif // __LIB_COLOR__
