/*
 * $File: scene.hxx
 * $Date: Wed Jun 20 12:40:12 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */


#ifndef __SCENE_HEADER__
#define __SCENE_HEADER__

#include <common.hxx>

#include <vector>

class RayTracing;

const int AA_NONE = 0,
	  AA_CFAA = 1, // Custom Filter Anti-Aliasing
	  AA_FSAA = 2; // Full Screen Anti-Aliasing

class Scene
{
	protected:

		ImageRenderer image_renderer;

	public:

		friend class RayTracing;

		class Frustum
		{
			public:
				real_t left, right, bottom, top, dist;
				Frustum(){}
				Frustum(real_t left, real_t right,
						real_t bottom, real_t top,
						real_t dist)
					: left(left), right(right),
					bottom(bottom), top(top), dist(dist) {}
		};

		// XXX
//	protected:
	public:


		Vector view_pos;
		Vector view_dir;
		Vector up_dir;

		std::vector<RenderablePrimitive *> renderable;
		std::vector<LightSource> light_source;

		Intensity ambient;

		bool enable_shadow;

		int anti_aliasing;
		
		real_t aa_cfaa_coef;

		bool enable_medium_density;
		
		Frustum frustum;

	public:
		
		Scene();
		~Scene();

		int width() const;
		int height() const;

		void addRenderable(RenderablePrimitive *obj);
		void addLightSource(const LightSource &light);
		void addLightSource(real_t x, real_t y, real_t z,
				real_t red, real_t green, real_t blue);
		void addLightSource(real_t x, real_t y, real_t z,
				const Intensity &intensity);

		void setView(const Vector &view_pos, 
				const Vector &view_dir, const Vector &up_dir);

		// in pixel
		void setImageResolution(int width, int height);

		void setFrustum(real_t left, real_t right,
				real_t bottom, real_t top,
				real_t dist);

		void setShadow(bool enable_shadow);
		void setAmbient(real_t red, real_t green, real_t blue);
		void setAmbient(const Intensity &ambient);
		void setAntiAliasing(int type);
		void setCFAACoefficient(real_t coef);
		void setEnableMediumDensity(bool state);

		void display();

		ImageRenderer *getImageRenderer()
		{ return &image_renderer; }
		void write(const char *fname);

		void clearRenderable();
};

#endif // __SCENE_HEADER__
