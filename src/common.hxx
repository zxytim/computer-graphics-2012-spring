/*
 * $File: common.hxx
 * $Date: Wed May 02 19:38:15 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_COMMON__
#define __INCLUDE_COMMON__

#include "lib/exception.hxx"
#include "lib/sassert.hxx"
#include "lib/auto_ptr.hxx"
#include "lib/image.hxx"
#include "lib/math.hxx"
#include "lib/timing.hxx"
//#include "lib/binder.hxx"
#include "lib/global.hxx"

#include "include/prototype.hxx"
#include "include/renderable.hxx"

#include "include/geometry.hxx"
#include "include/material.hxx"
#include "include/bdobject.hxx"

#endif // __INCLUDE_COMMON__
