/*
 * $File: setting.hxx
 * $Date: Tue May 01 22:21:55 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __SETTING_HEADER__
#define __SETTING_HEADER__

#include <global.hxx>

const real_t REFRACTIVE_INDEX_AIR = 1;
const real_t DENSITY_AIR = 0.01;

#endif // __SETTING_HEADER__
