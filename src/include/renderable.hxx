/*
 * $File: renderable.hxx
 * $Date: Thu Jun 07 23:51:40 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __RENDERABLE_PRIMITIVE__
#define __RENDERABLE_PRIMITIVE__

#include <prototype.hxx>
#include <material.hxx>
#include <geometry.hxx>
#include <bdobject.hxx>

class RayTracing;

/**
 * object that should be taken into account when do ray tracing rendering
 */

class RenderablePrimitive
{
	// XXX
	//protected:

	public:

		friend class RayTracing;

		AutoPtr<GeometryPrimitive> geometry;
		
		AutoPtr<Material> material;

		AutoPtr<BDObjectPrimitive> bdobject;

		string m_name;

	public:

		RenderablePrimitive(
				AutoPtr<GeometryPrimitive> geometry,
				AutoPtr<Material> material,
				string name = "Unknown renderable",
				AutoPtr<BDObjectPrimitive> bdobject = new BDObjectDummy());

		virtual ~RenderablePrimitive() {}

		/**
		 * return NULL if not intersect
		 */
		AutoPtr<RayTask> intersectionTest(const Ray &ray);

		// Texture
		AutoPtr<SurfaceProperty> getSurfaceProperty(AutoPtr<RayTask> &ray_task);


		// used by kd-tree
		inline bool isDefinite() { return geometry->isDefinite(); }

		inline void getCoordinateRange(
				real_t coord_low[3], real_t coord_high[3])
		{ return geometry->getCoordinateRange(coord_low, coord_high); }


		const string &name();
};

#endif // RenderablePrimitive
