/*
 * $File: AABox.cxx
 * $Date: Fri Jun 08 14:39:29 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "AABox.hxx"

#include <geometry/plane.hxx>
#include <cstring>

BDObjectAABox::BDObjectAABox()
{
}

BDObjectAABox::BDObjectAABox(const std::vector<Vector> &vtx)
{
	real_t x_min = REAL_INFINITY, x_max = -REAL_INFINITY,
		   y_min = REAL_INFINITY, y_max = -REAL_INFINITY,
		   z_min = REAL_INFINITY, z_max = -REAL_INFINITY;

	for (auto &v: vtx)
	{
		x_min = MIN(x_min, v.x);
		x_max = MAX(x_max, v.x);

		y_min = MIN(y_min, v.y);
		y_max = MAX(y_max, v.y);

		z_min = MIN(z_min, v.z);
		z_max = MAX(z_max, v.z);
	}

	real_t coord_lo[3], coord_hi[3];
	coord_lo[0] = x_min, coord_lo[1] = y_min, coord_lo[2] = z_min;
	coord_hi[0] = x_max, coord_hi[1] = y_max, coord_hi[2] = z_max;
	
	crange.set(coord_lo, coord_hi);
}


namespace BDObjectAABoxImpl
{
	struct AAPlane : public Plane
	{
		int cid;
		real_t coord;
		void set(int cid_, real_t coord_)
		{
			cid = cid_, coord = coord_;
			real_t *bs = reinterpret_cast<real_t *>(&base);
			real_t *nml = reinterpret_cast<real_t *>(&normal);
			for (int i = 0; i < 3; i ++)
				bs[i] = nml[i] = 0;
			bs[cid] = coord;
			nml[cid] = 1;
		}
	};
}

using namespace BDObjectAABoxImpl;

bool BDObjectAABox::BDIntersectionTest(const Ray &ray)
{
	AAPlane planes[6];

	for (int i = 0; i < 3; i ++)
	{
		planes[i * 2].set(i, crange.lo[i]);
		planes[i * 2 + 1].set(i, crange.hi[i]);
	}
	

	for (int i = 0; i < 6; i ++)
	{
		auto ray_task = planes[i].intersectionTest(ray);
		if (ray_task.ptr() != NULL)
		{
			Vector inter = ray_task->getIntersection();

			if (!crange.inside(inter, EPS))
				continue;
			return true;
		}
	}

	return false;
}

