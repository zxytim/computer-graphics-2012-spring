/*
 * $File: bdobject_primitive.hxx
 * $Date: Mon Apr 30 12:57:56 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __BDOBJECT_PRIMITIVE_HEADER__
#define __BDOBJECT_PRIMITIVE_HEADER__

#include "../prototype.hxx"

/**
 * bounding object is object that used
 * for fast intersect detection
 */
class BDObjectPrimitive
{
	public:

		virtual ~BDObjectPrimitive() {
		}

		virtual bool BDIntersectionTest(
				const Ray &ray) = 0;
};

#endif // __BDOBJECT_PRIMITIVE_HEADER__
