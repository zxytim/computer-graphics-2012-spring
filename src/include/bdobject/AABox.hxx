/*
 * $File: AABox.hxx
 * $Date: Fri Jun 08 14:39:16 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __BDOBJECT_AABOX__
#define __BDOBJECT_AABOX__

#include "bdobject_primitive.hxx"
#include <vector>

#include <cstring>
class BDObjectAABox : public BDObjectPrimitive
{
	protected:

		struct CoordRange
		{
			real_t lo[3];
			real_t hi[3];

			void set(real_t lo[3], real_t hi[3])
			{
				memcpy(this->lo, lo, sizeof(real_t) * 3);
				memcpy(this->hi, hi, sizeof(real_t) * 3);
			}

			bool inside(const Vector &p, real_t eps) const
			{
				return p.x >= lo[0] - eps && p.y >= lo[1] - eps 
					&& p.z >= lo[2] - eps
					&& p.x <= hi[0] + eps && p.y <= hi[1] + eps
					&& p.z <= hi[2] + eps;
			}
		};

		CoordRange crange;
	public:
		BDObjectAABox();
		BDObjectAABox(const std::vector<Vector> &vtx);

		void init(const std::vector<Vector> &vtx);

		virtual bool BDIntersectionTest(const Ray &ray);
};

#endif // __BDOBJECT_AABOX__
