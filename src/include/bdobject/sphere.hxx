/*
 * $File: sphere.hxx
 * $Date: Sun Apr 29 22:57:02 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __BDOBJECT_SPHERE__
#define __BDOBJECT_SPHERE__

#include "bdobject_primitive.hxx"
#include "../geometry/sphere.hxx"
#include <vector>

class BDObjectSphere : public BDObjectPrimitive, Sphere
{
	public:
		BDObjectSphere();
		BDObjectSphere(const std::vector<Vector> &vtx);

		void init(const std::vector<Vector> &vtx);

		virtual bool BDIntersectionTest(const Ray &ray);
};

#endif // __BDOBJECT_SPHERE__
