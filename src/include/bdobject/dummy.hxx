/*
 * $File: dummy.hxx
 * $Date: Sun Apr 29 22:59:29 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __BDOBJECT_DUMMY_HEADER__
#define __BDOBJECT_DUMMY_HEADER__

#include "bdobject_primitive.hxx"

class BDObjectDummy : public BDObjectPrimitive
{
	public:
		bool BDIntersectionTest(const Ray &ray);
};

#endif // __BDOBJECT_DUMMY_HEADER__
