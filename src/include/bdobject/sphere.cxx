/*
 * $File: sphere.cxx
 * $Date: Sun Apr 29 23:14:11 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "sphere.hxx"


BDObjectSphere::BDObjectSphere()
{
}

BDObjectSphere::BDObjectSphere(const std::vector<Vector> &vtx)
{
	init(vtx);
}

void BDObjectSphere::init(const std::vector<Vector> &vtx)
{
	real_t x_min = REAL_INFINITY, x_max = -REAL_INFINITY,
		   y_min = REAL_INFINITY, y_max = -REAL_INFINITY,
		   z_min = REAL_INFINITY, z_max = -REAL_INFINITY;

	for (auto &v: vtx)
	{
		x_min = MIN(x_min, v.x);
		x_max = MAX(x_max, v.x);

		y_min = MIN(y_min, v.y);
		y_max = MAX(y_max, v.y);

		z_min = MIN(z_min, v.z);
		z_max = MAX(z_max, v.z);
	}

	center = Vector(
			(x_min + x_max) * 0.5,
			(y_min + y_max) * 0.5,
			(z_min + z_max) * 0.5);

	radius = MAX(x_max - x_min, 
			MAX(y_max - y_min, z_max - z_min));

	north = Vector(0, 1, 0);
}


bool BDObjectSphere::BDIntersectionTest(const Ray &ray)
{
	return Sphere::intersectionTest(ray).ptr() != NULL;
}


