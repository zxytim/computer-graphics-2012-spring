/*
 * $File: material.hxx
 * $Date: Sat Apr 28 02:18:02 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __MATERIAL_HEADER_INNER__
#define __MATERIAL_HEADER_INNER__

#include <auto_ptr.hxx>
#include "texture.hxx"

class Material
{
	public:
		// Intensity related
		AutoPtr<Texture> texture;
		real_t shininess;


		// Physical attributes
		real_t refractive_index,
			   density;

		Material(AutoPtr<Texture> texture,
				real_t shininess = 5,
				real_t refractive_index = 1.2,
				real_t density = 0);
};

#endif // __MATERIAL_HEADER_INNER__
