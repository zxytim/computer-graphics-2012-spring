/*
 * $File: texture.hxx
 * $Date: Tue May 01 15:25:11 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */


#ifndef __HEADER_TEXTURE__
#define __HEADER_TEXTURE__

#include "texture/texture.hxx"
#include "texture/mono.hxx"
#include "texture/bitmap.hxx"
#include "texture/sgrid.hxx"

#endif // __HEADER_TEXTURE__
