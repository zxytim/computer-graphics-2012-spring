/*
 * $File: mono.cxx
 * $Date: Fri Apr 27 15:52:45 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */


#include <color.hxx>
#include "mono.hxx"
TextureMono::TextureMono(const char *color,
		const LightBlendCoefficient &blend,
		const LightEnergyCoefficient &energy) :
	TextureHomogenous(blend, energy),
	intensity(Color(color)) {}

AutoPtr<SurfaceProperty> TextureMono::getSurfaceProperty(
		real_t x, real_t y)
{
	return new SurfaceProperty(intensity, blend, energy);
}
