/*
 * $File: texture.hxx
 * $Date: Fri Apr 27 09:18:09 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __HEADER_TEXTURE_PRIMITIVE__
#define __HEADER_TEXTURE_PRIMITIVE__

#include <auto_ptr.hxx>
#include "../surface.hxx"

/**
 * fetch for detailed parameters for
 * surfaceProperty
 */
class Texture
{
	public:
		virtual AutoPtr<SurfaceProperty> getSurfaceProperty(real_t x, real_t y) = 0;
		virtual ~Texture() {};
};



#endif // __HEADER_TEXTURE_PRIMITIVE__
