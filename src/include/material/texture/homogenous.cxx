/*
 * $File: homogenous.cxx
 * $Date: Thu Apr 26 23:17:34 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "homogenous.hxx"

TextureHomogenous::TextureHomogenous(
		const Intensity &ambient,
		const Intensity &diffuse,
		const Intensity &specular,
		const Intensity &light_energy,
		const Intensity &reflect_energy,
		const Intensity &refract_energy) :
	blend(ambient, diffuse, specular),
	energy(light_energy, reflect_energy, refract_energy)
{
}


TextureHomogenous::TextureHomogenous(
		const LightBlendCoefficient &blend,
		const LightEnergyCoefficient &energy) :
	blend(blend), energy(energy)
{
}

