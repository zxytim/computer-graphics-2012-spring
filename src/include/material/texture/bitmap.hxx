/*
 * $File: bitmap.hxx
 * $Date: Mon Apr 30 02:06:06 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __TEXTURE_BITMAP__
#define __TEXTURE_BITMAP__

#include "homogenous.hxx"
#include <vector>

class TextureBitmap: public TextureHomogenous
{
	protected:
		int m_width, m_height;
		std::vector<Intensity> intensity;

		real_t x_scale, y_scale;

	public:
		// load from file
		TextureBitmap(const char *fname,
				real_t x_scale = 1.0, real_t y_scale = 1.0,
				const LightBlendCoefficient &blend
					= LightBlendCoefficient(1.0, 1.0, 1.0),
				const LightEnergyCoefficient &energy
					= LightEnergyCoefficient(1.0, 0.0, 0.0));

		int width();
		int height();
		AutoPtr<SurfaceProperty>
			getSurfaceProperty(real_t x, real_t y);
};

#endif // __TEXTURE_BITMAP__
