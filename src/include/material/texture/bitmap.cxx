/*
 * $File: bitmap.cxx
 * $Date: Mon Apr 30 21:49:30 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "bitmap.hxx"
#include <sassert.hxx>

#include <image.hxx>

TextureBitmap::TextureBitmap(
		const char *fname,
		real_t x_scale, real_t y_scale,
		const LightBlendCoefficient &blend,
		const LightEnergyCoefficient &energy) :
	TextureHomogenous(blend, energy),
	x_scale(x_scale), y_scale(y_scale)
{
	ImageRenderer image(fname);

	m_width = image.width();
	m_height = image.height();

	for (int i = 0; i < m_width; i ++)
		for (int j = 0; j < m_height; j ++)
		{
			Color col = image.pixelColor(i, j);
			intensity.push_back(col);
		}
}

AutoPtr<SurfaceProperty> TextureBitmap::getSurfaceProperty(
		real_t x_coord, real_t y_coord)
{
	x_coord /= x_scale;
	y_coord /= y_scale;

	long long x = x_coord * m_width,
		 y = y_coord * m_height;

	x %= m_width;
	y %= m_height;
	if (x < 0)
		x += m_width;
	if (y < 0)
		y += m_height;

	return new SurfaceProperty(intensity[x * m_height + y], blend, energy);
}

int TextureBitmap::width()
{
	return m_width;
}

int TextureBitmap::height()
{
	return m_height;
}

