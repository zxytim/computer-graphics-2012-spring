/*
 * $File: sgrid.cxx
 * $Date: Tue May 01 15:54:09 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "sgrid.hxx"

TextureSimpleGrid::TextureSimpleGrid(
		const SurfaceProperty &suf_0,
		const SurfaceProperty &suf_1,
		real_t x_scale, real_t y_scale) :
	suf_0(suf_0), suf_1(suf_1),
	x_scale(x_scale), y_scale(y_scale)
{
}

AutoPtr<SurfaceProperty> TextureSimpleGrid::getSurfaceProperty(real_t x, real_t y)
{
	long long x_coord = (long long)floor(x / x_scale),
		 y_coord = (long long)floor(y / y_scale);
	
	return ((x_coord + y_coord) & 1) ? new SurfaceProperty(suf_0) :
		new SurfaceProperty(suf_1);
}

