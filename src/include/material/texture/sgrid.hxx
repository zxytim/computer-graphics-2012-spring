/*
 * $File: sgrid.hxx
 * $Date: Tue May 01 15:26:07 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __TEXTURE_GRID__
#define __TEXTURE_GRID__

#include "texture.hxx"

class TextureSimpleGrid : public Texture
{
	protected:
		SurfaceProperty suf_0, suf_1;
		real_t x_scale, y_scale;
	public:
		TextureSimpleGrid(const SurfaceProperty &suf_0,
				const SurfaceProperty &suf_1,
				real_t x_scale, real_t y_scale);

		virtual AutoPtr<SurfaceProperty> getSurfaceProperty(real_t x, real_t y);
}; 

#endif // __TEXTURE_GRID__
