/*
 * $File: homogenous.hxx
 * $Date: Fri Apr 27 09:16:12 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

/*
 * Homogenous Texture is defined as:
 *		texture that has the same coefficient with
 *		respect to 
 *
 *			ambient, diffuse, specular,
 *			light energy, reflect energy, 
 *			refract energy
 *
 *		everywhere.
 */

#ifndef __TEXTURE_HOMOGENOUS__
#define __TEXTURE_HOMOGENOUS__

#include "texture.hxx"

class TextureHomogenous : public Texture 
{
	public:
		LightBlendCoefficient blend;
		LightEnergyCoefficient energy;

		TextureHomogenous(
				const Intensity &ambient,
				const Intensity &diffuse,
				const Intensity &specular,
				const Intensity &light_energy,
				const Intensity &reflect_energy,
				const Intensity &refract_energy);

		TextureHomogenous(
				const LightBlendCoefficient &blend,
				const LightEnergyCoefficient &energy);
		virtual AutoPtr<SurfaceProperty>
			getSurfaceProperty(real_t x, real_t y) = 0;
};

#endif // __TEXTURE_HOMOGENOUS__
