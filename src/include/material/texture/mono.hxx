/*
 * $File: mono.hxx
 * $Date: Fri Apr 27 15:52:28 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __TEXTURE_MONO__
#define __TEXTURE_MONO__

#include "homogenous.hxx"

class TextureMono : public TextureHomogenous
{
	public:
		Intensity intensity;

		TextureMono(real_t red, real_t green, real_t blue,
				const LightBlendCoefficient &blend,
				const LightEnergyCoefficient &energy) :
			TextureHomogenous(blend, energy),
			intensity(red, green, blue){}

		// x11 specified color name
		TextureMono(const char *color,
				const LightBlendCoefficient &blend,
				const LightEnergyCoefficient &energy);

		TextureMono(const Intensity &intensity,
				const LightBlendCoefficient &blend,
				const LightEnergyCoefficient &energy) :
			TextureHomogenous(blend, energy),
			intensity(intensity) {}

		AutoPtr<SurfaceProperty>
			getSurfaceProperty(real_t x, real_t y);
};

#endif // __TEXTURE_MONO__
