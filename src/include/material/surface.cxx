/*
 * $File: surface.cxx
 * $Date: Thu Apr 26 23:50:59 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "surface.hxx"

LightBlendCoefficient::LightBlendCoefficient(
		const Intensity &ambient,
		const Intensity &diffuse,
		const Intensity &specular) :
	ambient(ambient), diffuse(diffuse), specular(specular)
{
}

LightEnergyCoefficient::LightEnergyCoefficient(
		const Intensity &light,
		const Intensity &reflect,
		const Intensity &refract) :
	light(light), reflect(reflect), refract(refract)
{
}

SurfaceProperty::SurfaceProperty(
		const Intensity &base_intensity,
		const Intensity &ambient,
		const Intensity &diffuse,
		const Intensity &specular,
		const Intensity &light_energy,
		const Intensity &reflect_energy,
		const Intensity &refract_energy) :
	base_intensity(base_intensity),
	blend(ambient, diffuse, specular),
	energy(light_energy, reflect_energy, refract_energy)
{}

SurfaceProperty::SurfaceProperty(
		const Intensity &base_intensity,
		const LightBlendCoefficient &blend,
		const LightEnergyCoefficient &energy) :
	base_intensity(base_intensity),
	blend(blend), energy(energy)
{
}


