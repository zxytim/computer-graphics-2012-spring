/*
 * $File: material.cxx
 * $Date: Fri Apr 27 00:12:38 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "material.hxx"


Material::Material(
		AutoPtr<Texture> texture,
		real_t shininess,
		real_t refractive_index,
		real_t density) :
	texture(texture),
	shininess(shininess),
	refractive_index(refractive_index),
	density(density)
{
}

