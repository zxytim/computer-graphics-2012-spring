/*
 * $File: surface.hxx
 * $Date: Fri Apr 27 00:09:32 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __SURFACE_HEADER__
#define __SURFACE_HEADER__

#include <auto_ptr.hxx>
#include "../prototype.hxx"

class LightBlendCoefficient
{
	public:
		Intensity ambient,
				  diffuse,
				  specular;
		LightBlendCoefficient(
				const Intensity &ambient,
				const Intensity &diffuse,
				const Intensity &specular);
};


// due to Energy Conservation Law:
//		Q_incoming = Q_reflect + Q_refract + Q_absorb
//
// this indicate that the sum of 
// the two coeficient below
// should normally be smaller than 1.0
//
// these there 'energy' coefficient
// is also the coefficient for
// blending these three kinds of lights
class LightEnergyCoefficient 
{
	public:
		Intensity light, 
				  reflect,
				  refract;
		LightEnergyCoefficient(
				const Intensity &light,
				const Intensity &reflect,
				const Intensity &refract);
};

/**
 * Texture free attributes
 * TODO XXX:
 *	speed up by using ** const pointer **
 */
class SurfaceProperty
{
	public:

		Intensity base_intensity;

		// scalar of light source
		LightBlendCoefficient blend;

		LightEnergyCoefficient energy;

		SurfaceProperty(
				const Intensity &base_intensity,
				const Intensity &ambient,
				const Intensity &diffuse,
				const Intensity &specular,
				const Intensity &light_energy,
				const Intensity &reflect_energy,
				const Intensity &refract_energy);

		SurfaceProperty(
				const Intensity &base_intensity,
				const LightBlendCoefficient &blend,
				const LightEnergyCoefficient &energy);

		virtual ~SurfaceProperty() {}
};

#endif // __SURFACE_HEADER__
