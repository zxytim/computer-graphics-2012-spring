/*
 * $File: bdobject.hxx
 * $Date: Fri Jun 08 14:39:50 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __BDOBJECT_HEADER__
#define __BDOBJECT_HEADER__

#include "bdobject/bdobject_primitive.hxx"
#include "bdobject/dummy.hxx"
#include "bdobject/sphere.hxx"
#include "bdobject/AABox.hxx"

#endif
