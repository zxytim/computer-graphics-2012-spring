/*
 * $File: sphere.hxx
 * $Date: Thu Jun 07 23:53:05 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __GEOMETRY_SPHERE__
#define __GEOMETRY_SPHERE__

#include "geometry_primitive.hxx"

class Sphere;

class SphereRayTask : public RayTask
{
	protected:

		friend class Sphere;

		Sphere *sphere;

		int start_pos;
		real_t t, tpsqr;


		bool cached;

		void calculate();

		real_t m_dist;
		Vector m_normal;

		SphereRayTask(
				const Ray &ray,
				Sphere *sphere,
				int start_pos,
				real_t t,
				real_t tpsqr);
		
	public:
		virtual real_t getDist();
		virtual Vector getNormal();
		virtual GeometryPrimitive *getGeometry();
		virtual bool shootInsideGeometry();
		virtual bool shootOutsideGeometry();
};

class Sphere : public GeometryPrimitive
{
	protected:

		friend class SphereRayTask;

		Vector center;
		real_t radius;

		Vector north;
		real_t psi_phase;

	public:
	
		Sphere();

		Sphere(const real_t &x, const real_t &y, const real_t &z,
				const real_t &radius, const Vector &north = Vector(0, 1, 0), real_t phase_theta = 0);

		Sphere(const Vector &center,
				const real_t &radius,
				const Vector &north = Vector(0, 1, 0), real_t psi_phase = 0);

		// return null if not intersected
		// bounding test needed to be applied
		AutoPtr<RayTask> intersectionTest(const Ray &ray);

		void getSurfaceCoordinate(AutoPtr<RayTask> ray_task, 
				real_t &x, real_t &y);

		bool isDefinite() { return true; }

		void getCoordinateRange(
				real_t coord_low[3], real_t coord_high[3]);

};

#endif // __GEOMETRY_SPHERE__
