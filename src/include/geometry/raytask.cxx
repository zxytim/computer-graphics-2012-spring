/*
* $File: raytask.cxx
* $Date: Mon Apr 30 11:57:28 2012 +0800
* $Author: Xinyu Zhou <zxytim@gmail.com>
*/

#include "raytask.hxx"

RayTask::RayTask(const Ray & ray) :
	ray(ray)
{}

Vector RayTask::getIntersection()
{
	return ray.start + ray.dir * getDist();
}
