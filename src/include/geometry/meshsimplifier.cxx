/*
 * $File: meshsimplifier.cxx
 * $Date: Wed Jun 20 13:27:49 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "meshsimplifier.hxx"

#include <limits>
#include <algorithm>
#include <set>

using namespace std;

std::vector<AutoPtr<Mesh>> MeshSimplifier::simplify(
		const std::vector<Vector> &vtx_input,
		const std::vector<FaceInput> &face_input,
		const std::vector<real_t> &ratio_seq_input)
{
	fprintf(stderr, "start simplification\n");

	vtx.resize(vtx_input.size());
	face.resize(face_input.size());
	
	for (size_t i = 0; i < vtx_input.size(); i ++)
	{
		auto &v = vtx[i];
		v.pos = vtx_input[i];
		v.id = i;
		v.exist = true;
	}
	
	for (size_t i = 0; i < face_input.size(); i ++)
	{
		auto &f = face[i];
		for (int j = 0; j < 3; j ++)
			f.vtx[j] = &vtx[face_input[i].v[j]];
		f.normal = face_input[i].normal;
		f.exist = true;
		f.id = i;
	}

	vector<int> nface_seq;
	for (auto r: ratio_seq_input)
		if (r >= 0 && r <= 1)
			nface_seq.push_back(min(face.size() - 4.0, r * face.size()));

	sort(nface_seq.begin(), nface_seq.end());

	for (auto &f: face)
		for (auto &v: f.vtx)
			v->addFace(&f);

	for (auto &v: vtx)
	{
		set<Vertex *> hash;
		for (auto &f: v.face)
			for (auto &vt: f->vtx)
				if (vt != &v)
					hash.insert(vt);
		for (auto vt: hash)
			v.addNeighbor(vt);
		
		v.computeCost();

		heap.push(HeapElem(&v));
	}

#ifdef __SIMPLIFIER_TEST
	checkMesh();
#endif

	vector<AutoPtr<Mesh>> ret;

	heap.size();

	int cnt = 0;

	fprintf(stderr, "original: nvtx=%d, nface=%d\n",
			(int)vtx.size(), (int)face.size());

	size_t now = 0;
	int n_removed_face = 0;
#ifdef __DEBUG_BUILD
	nelem_in_heap = heap.size();
#endif
	while (!heap.empty() && face.size() - n_removed_face >= 4)
	{
		cnt ++;
		if (cnt == 7740)
			int asdf = 0;
#ifdef __SIMPLIFIER_TEST
		sassert(heap.size() >= nelem_in_heap);
#endif 
		while (now < nface_seq.size() && n_removed_face >= nface_seq[now])
		{
			ret.push_back(generateMesh(nface_seq[now] / (real_t)face.size()));
			now ++;
		}
		if (now >= nface_seq.size())
			break;

		auto &h = heap.top(); heap.pop();

		Vertex *u = h.v,
			   *v = u->vtx_clps_to;

		if (!u->exist)
			continue;

		if (u->cost_version != h.cost_version)
			continue;

		if (v && v->exist == false)
		{
			assert(0);
			u->computeCost();
			heap.push(HeapElem(u));
			continue;
		}

		n_removed_face += collapse(u, v);
#ifdef __SIMPLIFIER_TEST
		checkMesh();
#endif
	}

#ifdef __DEBUG_BUILD
	sassert(countFaceExist() == face.size() - n_removed_face);
	countVertexExist();
#endif

	return ret;
}

int MeshSimplifier::collapse(Vertex *u, Vertex *v)
{
	if (u->id == 0)
		int adsf = 0;
	sassert(u->exist);

#ifdef __DEBUG_BUILD
	nelem_in_heap --;
#endif

	int ret = 0;
	u->exist = false;
	do
	{
		if (!v)
			break;

		for (auto &f: u->face)
		{
			sassert(f->exist);
			if (f->hasVertex(v))
			{
				f->exist = false;
				for (auto &vt: f->vtx)
				{
					if (vt == u)
						continue;
					sassert(vt->exist);
					vt->removeFace(f);
				}
				ret ++;
			}
			else 
			{
				f->replaceVertex(u, v);
				v->addFace(f);
			}
		}
	} while(0);
	// TODO: update normal of faces along with
	//		the vertices on these faces

	for (auto &nb: u->neighbor)
	{
		sassert(nb->exist);
		nb->removeNeighbor(u);
		if (nb != v)
		{
			if (!nb->hasNeighbor(v))
				nb->addNeighbor(v);
			if (!v->hasNeighbor(nb))
				v->addNeighbor(nb);
		}
		nb->computeCost();
		heap.push(HeapElem(nb));
	}
	v->computeCost();
	heap.push(HeapElem(v));

	u->clear();
	return ret;
}


bool MeshSimplifier::Vertex::operator < (const Vertex &v) const
{
	return cost > v.cost;
}

void MeshSimplifier::Vertex::computeCost()
{
	cost_version ++;
	vtx_clps_to = NULL;
	if (neighbor.size() == 0)
	{
		cost = -0.01;
		return;
	}

	cost = std::numeric_limits<real_t>::max();
	for (auto &v: neighbor)
	{
		if (!v->exist)
			continue;
		real_t val = computeEdgeCollapseCost(this, v);
		if (val < cost)
		{
			cost = val;
			vtx_clps_to = v;
		}
	}
}

real_t MeshSimplifier::computeEdgeCollapseCost(Vertex *u, Vertex *v)
{
	real_t cost = 0;

	list<Face *> sides;
	for (auto &f: u->face)
		if (f->exist && f->hasVertex(v))
			sides.push_back(f);

	for (auto &f: u->face)
	{
		if (!f->exist)
			continue;
		real_t val = 1;
		for (auto &s: sides)
			val = min(val, (1 - f->normal.dot(s->normal)) * 0.5);
		cost = max(cost ,val);
	}

	return cost * (u->pos - v->pos).length();
}

AutoPtr<Mesh> MeshSimplifier::generateMesh(real_t ratio)
{
	Mesh *mesh = new Mesh;
	vector<int> old2new(vtx.size(), -1);
	for (size_t i = 0; i < vtx.size(); i ++)
		if (vtx[i].exist)
		{
			auto &p = vtx[i].pos;
			old2new[i] = mesh->addVertex(p.x, p.y, p.z);
		}

	for (auto &f: face)
		if (f.exist)
		{
			real_t v[3];
			for (int i = 0; i < 3; i ++)
				v[i] = old2new[f.vtx[i]->id];
			mesh->addFace(v[0], v[1], v[2]);
		}

	fprintf(stderr, "simplified mesh: ratio=%.2lf nvtx=%d, nface=%d\n", 
			ratio, mesh->nvtx(), mesh->nface());
	return mesh;
}

bool MeshSimplifier::Face::hasVertex(Vertex *v)
{
	for (auto &vt: vtx)
		if (vt == v)
		{
			sassert(vt->exist);
			return true;
		}
	return false;
}

void MeshSimplifier::Face::replaceVertex(Vertex *vtx_old, Vertex *vtx_new)
{
	if (id == 76)
		int asdf = 0;
	sassert(vtx_new->exist);
	for (auto &v: vtx)
		if (v == vtx_old)
		{
			v = vtx_new;
			return;
		}
	assert(0);
}

void MeshSimplifier::checkMesh()
{
	static int cnt = 0;
	cnt ++;
	for (auto &v: vtx)
	{
		if (!v.exist)
			continue;
		for (auto &f: v.face)
		{
			assert(f->exist);
			assert(f->hasVertex(&v));
			for (auto &vt: f->vtx)
				if (vt != &v)
					assert(v.hasNeighbor(vt));
		}
		for (auto &nb: v.neighbor)
		{
			assert(nb->exist);
			assert(nb->hasNeighbor(&v));
		}
	}
	for (auto &f: face)
	{
		if (!f.exist)
			continue;
		for (auto &v: f.vtx)
		{
			assert(v->exist);
			assert(v->hasFace(&f));
		}
	}
}

bool MeshSimplifier::Vertex::hasNeighbor(Vertex *v)
{
	for (auto &nb: neighbor)
		if (nb == v)
			return true;
	return false;
}

bool MeshSimplifier::Vertex::hasFace(Face *f)
{
	for (auto &fa: face)
		if (fa == f)
			return true;
	return false;
}

int MeshSimplifier::countFaceExist()
{
	int ret = 0;
	for (auto &f: face)
		ret += f.exist;
	return ret;
}

int MeshSimplifier::countVertexExist()
{
	int ret = 0;
	for (auto &v: vtx)
		ret += v.exist;
	return ret;
}

