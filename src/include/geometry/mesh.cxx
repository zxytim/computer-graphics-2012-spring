/*
 * $File: mesh.cxx
 * $Date: Tue Jun 19 14:04:20 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "mesh.hxx"

#include <sassert.hxx>
#include <initializer_list>
#include <map>
#include <set>
#include <vector>

#include "meshsimplifier.hxx"

Mesh::Vertex::Vertex() :
	has_normal(false)
{
}

Mesh::Vertex::Vertex(const real_t &x, const real_t &y,
		const real_t &z) :
	position(x, y, z), has_normal(false)
{
}

Mesh::Mesh()
	: finished(false), 
	has_closed_surface(false),
	enable_normal_interpolation(false),
	enable_kdtree(-1)
{
}

int Mesh::find_min_id()
{
	if (vtx.size() == 0)
	{
		idhash.insert(0);
		cur_it = idhash.begin();
		return 0;
	}

	for (; ;)
	{
		auto next_it = cur_it;
		next_it ++;
		if (next_it == idhash.end())
		{
			idhash.insert((*cur_it) + 1);
			cur_it ++;
			break;
		}
		cur_it = next_it;
	}
	return *cur_it;
}

int Mesh::addVertex(
		const real_t &x, const real_t &y, const real_t &z,
		int id)
{
	if (id == -1)
		id = find_min_id();
	else 
	{
#ifdef __DEBUG_BUILD
		sassert(id >= 0);
#endif
		if (id < -1)
			return 0;
		if (idhash.find(id) != idhash.end())
			return VERTEXT_ADD_FAILURE;
		idhash.insert(id);
	}

	sassert(id >= 0);

	Vertex v(x, y, z);
	if (vtx.size() == (size_t)id)
		vtx.push_back(v);
	else
	{
		vtx.resize(id + 1);
		vtx[id] = v;
	}

	return id;
}

int Mesh::addVertexN(const real_t &x, const real_t &y, const real_t &z,
		const real_t &n_x, const real_t &n_y, const real_t &n_z,
		int id)
{
	id = addVertex(x, y, z, id);
	if (id == -1)
		return id;
	vtx[id].normal = Vector(n_x, n_y, n_z);
	vtx[id].has_normal = true;
	return id;
}

int Mesh::addVertex(const Vector &position, int id)
{
	return addVertex(position.x, position.y, position.z, id);
}

int Mesh::addFace(int v0, int v1, int v2)
{
#define VTX_EXISTS(id) (idhash.find(id) != idhash.end())

#ifdef __DEBUG_BUILD
	sassert(VTX_EXISTS(v0) && VTX_EXISTS(v1) && VTX_EXISTS(v2));
#endif

	if (!(VTX_EXISTS(v0) && VTX_EXISTS(v1) && VTX_EXISTS(v2)))
		return FACE_ADD_FAILURE;
	if (v0 == v1 || v0 == v2 || v1 == v2)
		return FACE_ADD_FAILURE;

	Face f;
	f.v0 = v0, f.v1 = v1, f.v2 = v2;
	f.normal = (vtx[v1].position - vtx[v0].position).cross(
			vtx[v2].position - vtx[v1].position);
	face.push_back(f);

	return face.size() - 1;
}

void Mesh::addEdge(int v0, int v1)
{
	int mask = 1; 
	if (v0 > v1) {int t = v0; v0 = v1; v1 = t; mask = 2;} 
	unsigned long long id = v0 * (unsigned long long)vtx.size() + v1; 
	auto it = hash.find(id); 
	if (it == hash.end()) 
		hash[id] |= mask; 
	else 
	{ 
		if ((it->second & mask)) 
			throw 0; 
		it->second |= mask; 
	} 
}

void Mesh::finish(bool force)
{
	if (!force)
	{
		sassert(!finished);
	}

	// XXX: constructing edges

	// constructing vertex: face list
	for (size_t f = 0; f < face.size(); f ++)
		for (int v: {face[f].v0, face[f].v1, face[f].v2})
			vtx[v].face.push_back(f);

	for (auto &f: face)
	{
		f.normal = (vtx[f.v1].position - vtx[f.v0].position).cross(
				vtx[f.v2].position - vtx[f.v1].position);
		f.normal.unitize();
	}
	// calculate vertex normal using 
	// angle interpolation
	int vid = 0;
	for (auto &v: vtx)
	{
		if (v.has_normal)
			continue;

		v.face[0];
		auto &n = v.normal;
		n = Vector(0, 0, 0);

		std::vector<real_t> angle;
		real_t angle_sum = 0;
		int v0 = vid ++;
		// interpolate by angle
		for (int &fid: v.face)
		{
			auto &f = face[fid];
			int v1, v2;

			if (f.v0 == v0)
				v1 = f.v1, v2 = f.v2;
			else if (f.v1 == v0)
				v1 = f.v2, v2 = f.v0;
			else if (f.v2 == v0)
				v1 = f.v0, v2 = f.v1;
			else sassert(0);

			real_t a = (vtx[v1].position - vtx[v2].position).length(),
				   b = (vtx[v0].position - vtx[v1].position).length(),
				   c = (vtx[v0].position - vtx[v2].position).length();

			real_t alpha = acos((b * b + c * c - a * a) / (2 * b * c));
			angle.push_back(alpha);
			angle_sum += alpha;
		}
		int cnt = 0;
		for (int &fid: v.face)
		{
			auto &f = face[fid];
			n += f.normal * (angle[cnt ++] / angle_sum);
		}

		n.unitize();
	}

	// surface_closed-manifold check:
	//		1. sudden change of normal vector:
	//			a) mobius strip
	//			b) unintentionally mistakened input
	//		2. XXX: IGNORING self-pay faces
	try
	{
		//std::map<unsigned long long, int> hash;

		for (auto &f: face)
		{
#define ADD_EDGE(__v0, __v1) \
			do{ \
				int v0 = (__v0), v1 = (__v1); \
				int mask = 1; \
				if (v0 > v1) {int t = v0; v0 = v1; v1 = t; mask = 2;} \
				unsigned long long id = v0 * (unsigned long long)vtx.size() + v1; \
				auto it = hash.find(id); \
				if (it == hash.end()) \
				hash[id] |= mask; \
				else \
				{ \
					if ((it->second & mask)) \
					throw 0; \
					it->second |= mask; \
				} \
			} while (0)
			addEdge(f.v0, f.v1);
			addEdge(f.v1, f.v2);
			addEdge(f.v2, f.v0);
#undef ADD_EDGE
		}

		for (auto &i: hash)
			if (i.second != 3)
				throw 0;
		throw 1;
	}
	catch(int result)
	{
		has_closed_surface = result;
	}

#ifdef __DEBUG_BUILD
	fprintf(stderr, "has_closed_surface:%d\n", has_closed_surface);

#endif

	if (enable_kdtree == -1)
	{
		if (face.size() <= 20)
			enable_kdtree = false;
		else enable_kdtree = true;
	}

	if (enable_kdtree)
	{
		fprintf(stderr, "[Mesh] building up kd-tree...\n");
		kdface.resize(face.size());
		for (size_t i = 0; i < face.size(); i ++)
		{
			auto kdf = kdface[i] = new KdFace();
			kdf->fid = i;
			kdf->enable_normal_interpolation
				= this->enable_normal_interpolation;
			kdf->v[0] = vtx[face[i].v0];
			kdf->v[1] = vtx[face[i].v1];
			kdf->v[2] = vtx[face[i].v2];
			kdf->normal = face[i].normal;
		}

		kdtree.init(kdface);
	}
	finished = true;
}


void Mesh::setNoInside()
{
	has_closed_surface = false;
}

void Mesh::setNormalInterpolation(bool state)
{
	enable_normal_interpolation = state;
}

void Mesh::setKdTree(int state)
{
	enable_kdtree = state;
}

AutoPtr<RayTask> Mesh::forceIntersectionTest(const Ray &ray)
{

	struct FDN
	{
		Face *face;
		real_t dist;
		Vector normal;
	};

	FDN fdn;
	fdn.dist = REAL_INFINITY;
	fdn.face = NULL;


	for (auto &fa: face)
	{
		Vector &p0 = vtx[fa.v0].position,
			   &p1 = vtx[fa.v1].position,
			   &p2 = vtx[fa.v2].position;
		const Vector &d = ray.dir,
			  &o = ray.start;

		Vector e1 = p1 - p0,
			   e2 = p2 - p0;

		Vector q = d.cross(e2);
		real_t f = e1.dot(q);
		if (fabs(f) < GEPS)
			continue;

		f = 1 / f;
		Vector s = o - p0;
		real_t u = f * s.dot(q);
		if (u < 0)
			continue;
		Vector r = s.cross(e1);
		real_t v = f * d.dot(r);
		if (v < 0 || u + v > 1.0)
			continue;
		real_t dist = f * e2.dot(r);

		if (dist < GEPS)
			continue;

		if (dist < fdn.dist)
		{
			Vector normal;
			if (!enable_normal_interpolation)
				normal = fa.normal;
			else
				normal = 
					(1 - u - v) * vtx[fa.v0].normal
					+ u * vtx[fa.v1].normal
					+ v * vtx[fa.v2].normal;

			if (normal.dot(ray.dir) > 0)
				normal *= -1;
			if (normal.dot(ray.dir) >= -GEPS)
				continue;

			fdn.normal = normal;
			fdn.dist = dist;
			fdn.face = &fa;
		}
	}

	if (fdn.face == NULL)
		return NULL;

	return new MeshRayTask(ray, this, fdn.face, fdn.dist, fdn.normal);
}

AutoPtr<RayTask> Mesh::intersectionTest(const Ray &ray)
{
	sassert(finished);

	if (enable_kdtree)
	{
#ifdef __DEBUG_BUILD
		static int cnt = 0;
		cnt ++;
		if (cnt == 27552)
			int asdf = 0;
#endif

		auto inter = kdtree.getIntersection(ray);
		if (inter.obj == NULL)
			return NULL;
#ifdef __DEBUG_TEST_KD_TREE

		AutoPtr<RayTask> kdrt = new MeshRayTask(ray, this, &face[inter.obj->fid],
				inter.ray_task->dist, inter.ray_task->normal),
			frt = forceIntersectionTest(ray);
		if (frt.ptr() == NULL)
			assert(kdrt.ptr() == NULL);
		else
		{
			double dkd = kdrt->getDist(),
				   df = frt->getDist();

			Vector a = kdrt->getIntersection(),
				   b = frt->getIntersection();

			if (fabs(dkd - df) >= EPS)
				int asdf = 0;

			assert(fabs(dkd - df) < EPS);
			assert(fabs((a - b).lengthsqr()) < EPS);
		}
		return kdrt;
#else
		return new MeshRayTask(ray, this, &face[inter.obj->fid],
				inter.ray_task->dist, inter.ray_task->normal);
#endif
	}
	else return forceIntersectionTest(ray);
}

void Mesh::getSurfaceCoordinate(
		AutoPtr<RayTask> ray_task, 
		real_t &x, real_t &y)
{
	// XXX
	x = y = 0;
}


void Mesh::getCoordinateRange(
		real_t coord_low[3], real_t coord_high[3])
{
	for (int i = 0; i < 3; i ++)
	{
		coord_low[i] = REAL_INFINITY;
		coord_high[i] = -REAL_INFINITY;
	}

	for (auto &f: kdface)
	{
		real_t lo[3], hi[3];
		f->getCoordinateRange(lo, hi);
		for (int i = 0; i < 3; i ++)
		{
			coord_low[i] = MIN(coord_low[i], lo[i]);
			coord_high[i] = MAX(coord_high[i], hi[i]);
		}
	}
}

void Mesh::translate(const real_t &x, const real_t &y, const real_t &z)
{
	sassert(!finished);

	Vector delta(x, y, z);
	for (auto &v: vtx)
		v.position += delta;
}

namespace MeshImpl
{
	void rotate(real_t &x, real_t &y,
			const real_t &c, const real_t &s)
	{
		real_t nx = x * c - y * s;
		real_t ny = x * s + y * c;
		x = nx, y = ny;
	}

}

void Mesh::rotate(real_t x_rad, real_t y_rad, real_t z_rad)
{
	real_t cx = cos(x_rad), sx = sin(x_rad),
		   cy = cos(y_rad), sy = sin(y_rad),
		   cz = cos(z_rad), sz = sin(z_rad);

	for (auto &v: vtx)
	{
		auto &p = v.position;
		MeshImpl::rotate(p.y, p.z, cx, sx); // x axis
		MeshImpl::rotate(p.z, p.x, cy, sy); // y axis
		MeshImpl::rotate(p.x, p.y, cz, sz); // z axis

		auto &n = v.normal;
		MeshImpl::rotate(n.y, n.z, cx, sx); // x axis
		MeshImpl::rotate(n.z, n.x, cy, sy); // y axis
		MeshImpl::rotate(n.x, n.y, cz, sz); // z axis
	}
}

void Mesh::rotatedeg(real_t x_deg, real_t y_deg, real_t z_deg)
{
	rotate(x_deg / 180 * M_PI, y_deg / 180 * M_PI, z_deg / 180 * M_PI);
}

void Mesh::scale(real_t x_factor, real_t y_factor, real_t z_factor)
{
	for (auto &v: vtx)
	{
		Vector &p = v.position;
		p.x *= x_factor;
		p.y *= y_factor;
		p.z *= z_factor;
	}
}

void Mesh::scale(real_t factor)
{
	for (auto &v: vtx)
		v.position *= factor;
}

real_t Mesh::normalize()
{
	sassert(nvtx() >= 3);
	real_t max_coord = 0;

	for (auto &v: vtx)
	{
		auto &p = v.position;
		real_t t = MAX(fabs(p.x), fabs(p.y));
		t = MAX(t, fabs(p.z));
		max_coord = MAX(max_coord, t);
	}

	real_t ratio = 1.0 / max_coord;
	for (auto &v: vtx)
		v.position *= ratio;

	return ratio;
}

void Mesh::translateToOrigin()
{
	Vector pos;
	for (auto &v: vtx)
		pos += v.position;
	pos = -pos / vtx.size();
	translate(pos.x, pos.y, pos.z);

}


int Mesh::nvtx() const
{
	return vtx.size();
}

int Mesh::nface() const
{
	return face.size();
}


void Mesh::getVertexVector(std::vector<Vector> &pts) const
{
	pts.clear();
	for (auto &v: vtx)
		pts.push_back(v.position);
}

// MeshRayTask

MeshRayTask::MeshRayTask(
		const Ray &ray, 
		Mesh *mesh,
		Mesh::Face *face,
		real_t dist, 
		const Vector &normal) :
	RayTask(ray), mesh(mesh),
	face(face),
	dist(dist), normal(normal)
{
}

real_t MeshRayTask::getDist()
{
	return dist;
}

Vector MeshRayTask::getNormal()
{
	return normal;
}


bool MeshRayTask::shootInsideGeometry()
{
	if (!mesh->has_closed_surface)
		return false;
	return normal.dot(face->normal) > GEPS;
}

bool MeshRayTask::shootOutsideGeometry()
{
	if (!mesh->has_closed_surface)
		return false;
	return normal.dot(face->normal) < -GEPS;
}


GeometryPrimitive *MeshRayTask::getGeometry()
{
	return mesh;
}


void Mesh::KdFace::getCoordinateRange(
		real_t coord_low[3], real_t coord_high[3])
{
#define UPDATE(i, sub) \
	do { \
		coord_low[i] = REAL_INFINITY; \
		coord_high[i] = -REAL_INFINITY; \
		for (int j = 0; j < 3; j ++) \
		{ \
			coord_low[i] = MIN(coord_low[i], v[j].position.sub);\
			coord_high[i] = MAX(coord_high[i], v[j].position.sub);\
		} \
	} while(0) 

	UPDATE(0, x);
	UPDATE(1, y);
	UPDATE(2, z);

}

AutoPtr<Mesh::KdFaceRayTask> Mesh::KdFace::intersectionTest(const Ray &ray)
{
	Vector &p0 = this->v[0].position,
		   &p1 = this->v[1].position,
		   &p2 = this->v[2].position;
	const Vector &d = ray.dir,
		  &o = ray.start;

	Vector e1 = p1 - p0,
		   e2 = p2 - p0;

	Vector q = d.cross(e2);
	real_t f = e1.dot(q);
	if (fabs(f) < GEPS)
		return NULL;

	f = 1 / f;
	Vector s = o - p0;
	real_t u = f * s.dot(q);
	if (u < 0)
		return NULL;
	Vector r = s.cross(e1);
	real_t v = f * d.dot(r);
	if (v < 0 || u + v > 1.0)
		return NULL;
	real_t dist = f * e2.dot(r);

	if (dist < GEPS)
		return NULL;

	Vector normal;
	if (!enable_normal_interpolation)
		normal = this->normal;
	else
		normal = 
			(1 - u - v) * this->v[0].normal
			+ u * this->v[1].normal
			+ v * this->v[2].normal;

	if (normal.dot(ray.dir) > 0)
		normal *= -1;
	if (normal.dot(ray.dir) >= -GEPS)
		return NULL;

	return new KdFaceRayTask(ray, this, dist, normal);
}

Mesh::KdFaceRayTask::KdFaceRayTask(const Ray &ray, KdFace *kdface,
		real_t dist, const Vector &normal) :
	ray(ray),
	kdface(kdface),
	dist(dist), normal(normal)
{
}


real_t Mesh::KdFaceRayTask::getDist()
{
	return dist;
}

Vector Mesh::KdFaceRayTask::getIntersection()
{
	return ray.start + ray.dir * dist;
}

void Mesh::printToObjFile(FILE *file) const
{
	for (auto i: vtx)
		fprintf(file, "v %lf %lf %lf\n", i.position.x, i.position.y, i.position.z);
	for (size_t i = 0; i < face.size(); i ++)
	{
		auto &f = face[i];
		fprintf(file, "f %d %d %d\n", f.v0 + 1, f.v1 + 1, f.v2 + 1);
	}
} 

vector<AutoPtr<Mesh>> Mesh::simplify(const std::vector<real_t> &ratio_seq)
{
	assert(finished);
	std::vector<Vector> spts;
	std::vector<MeshSimplifier::FaceInput> sface;

	getVertexVector(spts);
	for (auto &f: face)
	{
		MeshSimplifier::FaceInput sf;
		sf.v[0] = f.v0;
		sf.v[1] = f.v1;
		sf.v[2] = f.v2;
		sf.normal = f.normal;
		sface.push_back(sf);
	}

	MeshSimplifier msimplifier;

	return msimplifier.simplify(spts, sface, ratio_seq);
}

