/*
 * $File: cuboid.hxx
 * $Date: Sun Apr 29 11:12:46 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __GEOMETRY_CUBOID__
#define __GEOMETRY_CUBOID__

#include "mesh.hxx"

class Cuboid : public Mesh
{
	protected:
		void addRect(int v0, int v1, int v2, int v3);
	public:
		// front direction is the cross product
		// of @up and @left
		// @c is height
		Cuboid(real_t a, real_t b, real_t c,
				const Vector &up = Vector(0, 1, 0), 
				const Vector &left = Vector(1, 0, 0));

};


#endif // __GEOMETRY_CUBOID__
