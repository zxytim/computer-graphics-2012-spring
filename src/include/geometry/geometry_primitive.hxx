/*
 * $File: geometry_primitive.hxx
 * $Date: Thu Jun 07 23:55:57 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __GEOMETRY_PRIMITIVE_HEADER_INNER__
#define __GEOMETRY_PRIMITIVE_HEADER_INNER__

#include "../prototype.hxx"
#include "raytask.hxx"
#include <auto_ptr.hxx>


/**
 * Use more precise epsilon
 */
const real_t GEPS = EPS * 0.1;

class GeometryPrimitive
{
	public:
		
		GeometryPrimitive();

		virtual ~GeometryPrimitive() {
		}

		// return nullptr if not intersected
		virtual AutoPtr<RayTask> intersectionTest(
				const Ray &ray) = 0;

		virtual void getSurfaceCoordinate(
				AutoPtr<RayTask> ray_task, 
				real_t &x, real_t &y) = 0;

		/**
		 * whether the geometry is definite, 
		 * which means the coordinate range in
		 * all three of the demensions 
		 * has a limit
		 *
		 * This attribute is used by 
		 * space partitioning algorithms
		 * that speed up the tracing procedure
		 */
		virtual bool isDefinite() { return false; }

		virtual void getCoordinateRange(
				real_t coord_low[3], real_t coord_high[3]);
};

#endif // __GEOMETRY_PRIMITIVE_HEADER_INNER__
