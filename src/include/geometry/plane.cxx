/*
* $File: plane.cxx
* $Date: Fri Jun 08 00:04:52 2012 +0800
* $Author: Xinyu Zhou <zxytim@gmail.com>
*/

#include "plane.hxx"

Plane::Plane(const Vector &base, const Vector &normal) :
	base(base), normal(normal), enable_uv_mapping(false)
{
	sassert(normal.lengthsqr() > GEPS);
	this->normal.unitize();
}

Plane::Plane(const real_t &base_x, const real_t &base_y, 
		const real_t &base_z, const real_t &normal_x, 
		const real_t &normal_y, const real_t &normal_z) :
	Plane(Vector(base_x, base_y, base_z), 
			Vector(normal_x, normal_y, normal_z))
{
}

Plane::Plane(const Vector &base, const Vector &normal,
		const Vector &x_dir) :
	base(base), normal(normal), x_dir(x_dir)
{
	sassert(normal.lengthsqr() > GEPS);
	sassert(x_dir.lengthsqr() > GEPS);
	this->normal.unitize();
	this->x_dir.unitize();

	enable_uv_mapping = true;
	sassert(fabs(x_dir.dot(normal)) < GEPS);
	y_dir = x_dir.cross(normal);
	y_dir.unitize();
}

Plane::Plane(const real_t &base_x, const real_t &base_y, 
		const real_t &base_z, const real_t &normal_x, 
		const real_t &normal_y, const real_t &normal_z,
		const real_t &x_dir_x, const real_t &x_dir_y,
		const real_t &x_dir_z) :
	Plane(Vector(base_x, base_y, base_z),
			Vector(normal_x, normal_y, normal_z),
			Vector(x_dir_x, x_dir_y, x_dir_z))
{
}

AutoPtr<RayTask> Plane::intersectionTest(const Ray &ray)
{
	Vector rnormal = normal; // normal with respect to ray
	if ((base - ray.start).dot(rnormal) > 0)
		rnormal = -rnormal;

	if (rnormal.dot(ray.dir) > -GEPS)
		return NULL;

	return new PlaneRayTask(ray, this, rnormal);
}


void Plane::getSurfaceCoordinate(
		AutoPtr<RayTask> ray_task, 
		real_t &x, real_t &y)
{
	Vector p = (ray_task->getIntersection() - base);
	x = x_dir.dot(p);
	y = y_dir.dot(p);
}

// PlaneRayTask

PlaneRayTask::PlaneRayTask(const Ray &ray, Plane *plane,
		const Vector &normal) :
	RayTask(ray), plane(plane),
	normal(normal), cached(false)
{
	sassert(normal.length() >= 0);
}

real_t PlaneRayTask::getDist()
{
	if (!cached)
	{
		Vector d0v = ray.start - plane->base;
		real_t d1 = d0v.dot(normal);
		sassert(d1 > -GEPS);
		real_t cos_a = -ray.dir.dot(normal);
		sassert(cos_a > GEPS);
		dist = d1 / cos_a;
		sassert(dist > 0);

		cached = true;

		// XXX
#ifdef __DEBUG_BUILD
		Vector intersection = ray.start + ray.dir * dist;

		// on plane
		sassert(fabs((intersection - plane->base).dot(normal)) < GEPS);

		// WRONG: the right direction
		// REASON: both direction is available
		//sassert(normal.dot(intersection - ray.start) < -GEPS);
#endif
	}
	return dist;
}

Vector PlaneRayTask::getNormal()
{
	return normal;
}


bool PlaneRayTask::shootInsideGeometry()
{
	return false;
}


bool PlaneRayTask::shootOutsideGeometry()
{
	return false;
}

GeometryPrimitive *PlaneRayTask::getGeometry()
{
	return plane;
}


