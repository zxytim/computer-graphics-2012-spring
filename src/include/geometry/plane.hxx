/*
* $File: plane.hxx
* $Date: Thu Jun 07 17:09:24 2012 +0800
* $Author: Xinyu Zhou <zxytim@gmail.com>
*/

#ifndef __GEOMETRY_PLANE__
#define __GEOMETRY_PLANE__

#include "geometry_primitive.hxx"

class PlaneRayTask;

class Plane : public GeometryPrimitive
{
	protected:

		friend class PlaneRayTask;

		Vector base, 
			   normal;

		Vector x_dir,
			   y_dir;
		bool enable_uv_mapping;

	public:
		
		Plane(){}
		Plane(const Vector &base, const Vector &normal);

		Plane(const Vector &base, const Vector &normal,
				const Vector &x_dir);

		Plane(const real_t &base_x, const real_t &base_y, 
				const real_t &base_z, const real_t &normal_x, 
				const real_t &normal_y, const real_t &normal_z);

		Plane(const real_t &base_x, const real_t &base_y, 
				const real_t &base_z, const real_t &normal_x, 
				const real_t &normal_y, const real_t &normal_z,
				const real_t &x_dir_x, const real_t &x_dir_y,
				const real_t &x_dir_z);

		virtual AutoPtr<RayTask> intersectionTest(const Ray &ray);

		virtual void getSurfaceCoordinate(
				AutoPtr<RayTask> ray_task, 
				real_t &x, real_t &y);

		//Vector
};

class PlaneRayTask : public RayTask
{
	protected:

		friend class Plane;

		Plane *plane;
		Vector normal; // normal with respect to ray 

		bool cached;
		real_t dist;

		PlaneRayTask(const Ray &ray, Plane *plane, 
				const Vector &normal);

	public:
		virtual real_t getDist();
		virtual Vector getNormal();
		virtual bool shootInsideGeometry();
		virtual bool shootOutsideGeometry();
		virtual GeometryPrimitive *getGeometry();
};
#endif // __GEOMETRY_PLANE__
