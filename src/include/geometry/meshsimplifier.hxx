/*
 * $File: meshsimplifier.hxx
 * $Date: Wed Jun 20 13:20:50 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __MESHSIMPLIFIER_HEADER__
#define __MESHSIMPLIFIER_HEADER__

#include "mesh.hxx"

#include <list>
#include <set>
#include <queue>
#include <vector>
#include <cstring>
#include <auto_ptr.hxx>

using std::list;
using std::vector;

class MeshSimplifier
{
	protected:
		class Vertex;

		class Face
		{
			public:
				bool exist;
				Vertex *vtx[3];
				Vector normal;

				int id;

				bool hasVertex(Vertex *v);
				void replaceVertex(Vertex *vtx_old, Vertex *vtx_new);
		};

		class Vertex
		{
			public:

				bool exist;

				int id;
				Vector pos;
				
				int cost_version;
				Vertex() : cost_version(0) {}

				std::set<Vertex *> neighbor;
				std::set<Face *> face;

				Vertex *vtx_clps_to;
				real_t cost;

				void addFace(Face *f)
				{
					sassert(face.find(f) == face.end());
					face.insert(f);
				}

				void removeFace(Face *f)
				{
					sassert(face.find(f) != face.end());
					face.erase(f);
				}

				void addNeighbor(Vertex *v)
				{
					sassert(neighbor.find(v) == neighbor.end());
					sassert(v != this);
					neighbor.insert(v);
				}
				void removeNeighbor(Vertex *v)
				{
					sassert(neighbor.find(v) != neighbor.end());
					neighbor.erase(v);
				}

				bool hasNeighbor(Vertex *v);
				bool hasFace(Face *f);

				void computeCost();
				bool operator < (const Vertex &v) const;

				void clear() { neighbor.clear(), face.clear(); }
		};

		struct HeapElem
		{
			Vertex *v;
			int cost_version;
			real_t cost;

			HeapElem(Vertex *v) :
				v(v), cost_version(v->cost_version), cost(v->cost) {}

			bool operator < (const HeapElem &h) const
			{ return cost > h.cost; }
		};

		typedef std::priority_queue<HeapElem> Heap;

		static real_t computeEdgeCollapseCost(Vertex *u, Vertex *v);

		// return number of faces removed
		int collapse(Vertex *u, Vertex *v);

		void checkMesh();

		vector<Vertex> vtx;
		vector<Face> face;

		Heap heap;

#ifdef __DEBUG_BUILD
		int nelem_in_heap;
#endif

		AutoPtr<Mesh> generateMesh(real_t ratio);

		int countFaceExist();
		int countVertexExist();

	public:

		struct FaceInput
		{
			int v[3];
			Vector normal;
		};

		vector<AutoPtr<Mesh>> simplify(
				const std::vector<Vector> &vtx,
				const std::vector<FaceInput> &face,
				const std::vector<real_t> &ratio_seq);
};

#endif // __MESHSIMPLIFIER_HEADER__
