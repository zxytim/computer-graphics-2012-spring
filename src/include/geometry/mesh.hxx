/*
 * $File: mesh.hxx
 * $Date: Tue Jun 19 14:04:47 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __GEOMETRY_MESH__
#define __GEOMETRY_MESH__

#include "geometry_primitive.hxx"
#include <vector>
#include <set>

#include <kdtree.hxx>

class MeshRayTask;

/**
 * `Mesh` is defined as a manifold consists of
 * non-cross triangles. a single
 */
class Mesh : public GeometryPrimitive
{
	protected:

		friend class MeshRayTask;

		struct Vertex;
		struct Edge;
		struct Face;

		struct Vertex
		{
			Vector position;
			Vector normal;

			bool has_normal;

			// texture binding position
			Vector2D bind;

			Vertex();
			Vertex(const real_t &x, const real_t &y,
					const real_t &z);

			std::vector<int> face;
		};

		std::vector<Vertex> vtx;

		// XXX
#if 0
		struct Edge
		{
			int v0, v1;
			int f0, f1;
		}; 
		std::vector<Edge> edge;
#endif
		struct Face
		{
			int v0, v1, v2;
			int n0, n1, n2;

			// normal vector of face 
			// is defined as the cross product between
			// (v1 - v0) and (v2 - v1)
			Vector normal;

			//int e0, e1, e2;
		};

		class KdFaceRayTask;

		struct KdFace 
		{
			int fid;
			bool enable_normal_interpolation;
			Vertex v[3];
			Vector normal;

			virtual bool isDefinite() { return true; }
			virtual void getCoordinateRange(
					real_t coord_low[3], real_t coord_high[3]);

			virtual AutoPtr<KdFaceRayTask> intersectionTest(const Ray &ray);
			virtual ~KdFace() {}
		};


		class KdFaceRayTask 
		{
			public:
				Ray ray;

				KdFace *kdface;

				real_t dist;
				Vector normal;

				KdFaceRayTask(const Ray &ray, KdFace *kdface,
						real_t dist, const Vector &normal);

				virtual real_t getDist();
				virtual Vector getIntersection();
				virtual ~KdFaceRayTask() {}
		};

		KdTree<KdFace, KdFaceRayTask> kdtree;

		std::vector<Face> face;
		std::vector<KdFace *> kdface;

		std::set<int> idhash;

		std::set<int>::iterator cur_it;

		int find_min_id();

		bool finished;

		bool has_closed_surface;
		bool enable_normal_interpolation;
		int enable_kdtree;

		void addEdge(int v0, int v1);
		std::map<unsigned long long, int> hash;

	public:

		static const int VERTEXT_ADD_FAILURE = -1,
					 FACE_ADD_FAILURE = -1;

		Mesh();

		/**
		 * `id`s are integer numbers start from 0
		 *
		 * if @id equals -1, then the smallest absent number
		 * will be assigned to this vertex
		 * 
		 * return:
		 *		the id of this vertex 
		 */
		// TODO: texture binding
		int addVertex(const real_t &x, const real_t &y, const real_t &z,
				int id = -1);

		int addVertexN(const real_t &x, const real_t &y, const real_t &z,
				const real_t &n_x, const real_t &n_y, const real_t &n_z,
				int id = -1);
		int addVertex(const Vector &position, int id = -1);

		/**
		 * The id of the three vertex
		 * return:
		 *		the id of this face
		 */
		int addFace(int v0, int v1, int v2);

		// After finishing adding Vertices and Faces,
		// finish() **MUST** be invoked
		//
		// but after invoking finish(),
		// you can still translate this mesh
		void finish(bool force = false);

		// should be invoked after invoking finish()
		void setNoInside();
		void setNormalInterpolation(bool state);

		/**
		 * 0 for disable
		 * 1 for enable
		 * -1 for auto
		 */
		void setKdTree(int state);

		// the whole operation
		void translate(const real_t &x, const real_t &y, const real_t &z);
		void rotate(real_t x_rad, real_t y_rad, real_t z_rad);
		void rotatedeg(real_t x_deg, real_t y_deg, real_t z_deg);
		void scale(real_t x_factor, real_t y_factor, real_t z_factor);
		void scale(real_t factor);

		// scale the maximum absolute value of coordinate to 1.0
		// return the scale ratio
		real_t normalize();

		void translateToOrigin();

		int nvtx() const;
		int nface() const;

		void getVertexVector(std::vector<Vector> &pts) const;
		void printToObjFile(FILE *file) const;

		// inherited functions
		AutoPtr<RayTask> intersectionTest(const Ray &ray);

		AutoPtr<RayTask> forceIntersectionTest(const Ray &ray);

		virtual void getSurfaceCoordinate(
				AutoPtr<RayTask> ray_task, 
				real_t &x, real_t &y);

		virtual bool isDefinite() { return true; }
		virtual void getCoordinateRange(
				real_t coord_low[3], real_t coord_high[3]);


		/**
		 * return a unfinished mesh
		 */
		std::vector<AutoPtr<Mesh>> simplify(const std::vector<real_t> &ratio_seq);
};

class MeshRayTask : public RayTask
{
	protected:
		friend class Mesh;

		Mesh *mesh;
		Mesh::Face *face;

		real_t dist;
		Vector normal;

		MeshRayTask(const Ray &ray, Mesh *mesh,
				Mesh::Face *face,
				real_t dist, const Vector &normal);

	public:

		virtual real_t getDist();
		virtual Vector getNormal();

		virtual bool shootInsideGeometry();
		virtual bool shootOutsideGeometry();
		virtual GeometryPrimitive *getGeometry();

};

#endif // __GEOMETRY_MESH__
