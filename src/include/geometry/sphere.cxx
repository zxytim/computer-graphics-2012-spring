/*
 * $File: sphere.cxx
 * $Date: Fri Jun 08 19:30:10 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "sphere.hxx"
#include <sassert.hxx>


static const int IN_SPHERE = 0,
			 ON_BORDER = 1,
			 OUT_SPHERE = 2;

/**
 * Ray Task
 * {
 */

SphereRayTask::SphereRayTask(
		const Ray &ray,
		Sphere *sphere,
		int start_pos,
		real_t t,
		real_t tpsqr) :
	RayTask(ray), sphere(sphere),
	start_pos(start_pos),
	t(t), tpsqr(tpsqr), cached(false)
{
	sassert(fabs(ray.dir.lengthsqr() - 1) < GEPS);
}

void SphereRayTask::calculate()
{
	sassert(fabs(ray.dir.lengthsqr() - 1) < GEPS);
	tpsqr = sqrt(tpsqr);

	if (start_pos == IN_SPHERE || start_pos == ON_BORDER) 
		m_dist = t + tpsqr;
	else if (start_pos == OUT_SPHERE) 
		m_dist = t - tpsqr;
#ifdef __DEBUG_BUILD
	else sassert(0);
#endif

#ifdef __DEBUG_BUILD
	real_t tmp = fabs(((ray.start + m_dist * ray.dir) - sphere->center).lengthsqr() - sphere->radius * sphere->radius);
	sassert(tmp < GEPS);
#endif 

	m_normal = (ray.start + ray.dir * m_dist - sphere->center);

	if (start_pos == IN_SPHERE || start_pos == ON_BORDER)
		m_normal = -m_normal;

	sassert(ray.dir.dot(m_normal) < GEPS);
	cached = true;
}

Vector SphereRayTask::getNormal()
{
	if (!cached)
		calculate();
	return m_normal;
}

bool SphereRayTask::shootInsideGeometry()
{
	return (getIntersection() - sphere->center).dot(ray.dir) < -GEPS;
}


bool SphereRayTask::shootOutsideGeometry()
{
	return !shootInsideGeometry();
}

GeometryPrimitive *SphereRayTask::getGeometry()
{
	return sphere;
}

real_t SphereRayTask::getDist()
{
	if (!cached)
		calculate();
	return m_dist;
}


/**
 * }
 */

Sphere::Sphere()
{
}

Sphere::Sphere(
		const Vector &center,
		const real_t &radius,
		const Vector &north,
		real_t psi_phase) :
	center(center), radius(radius),
	north(north), psi_phase(psi_phase)
{
	this->north.unitize();
}


Sphere::Sphere(const real_t &x, const real_t &y, const real_t &z,
		const real_t &radius, const Vector &north, real_t psi_phase) :
	Sphere(Vector(x, y, z), radius, north, psi_phase)
{
}

AutoPtr<RayTask> Sphere::intersectionTest(const Ray &ray)
{
	sassert(fabs(ray.dir.lengthsqr() - 1) < GEPS);
	Vector l = center - ray.start;

	real_t rsqr = radius * radius;
	real_t lsqr = l.dot(l);

	int start_pos;

	if (lsqr < rsqr - GEPS) // in sphere
		start_pos = IN_SPHERE;
	else if (lsqr > rsqr + GEPS) // out sphere
		start_pos = OUT_SPHERE;
	else start_pos = ON_BORDER;

	real_t t = l.dot(ray.dir);

	// emit further
	if (t < -GEPS && (start_pos == OUT_SPHERE || start_pos == ON_BORDER))
		return NULL;

	real_t dsqr = l.dot(l) - t * t;
	// line and sphere not intersect
	if (dsqr > rsqr - GEPS)
		return NULL;

	SphereRayTask *ray_task = new SphereRayTask(ray, this, start_pos, t, rsqr - dsqr);

	return ray_task;
}

static void rotate(real_t &x, real_t &y, real_t c, real_t s)
{
	real_t nx = x * c - y * s,
		   ny = y * c + x * s;
	x = nx, y = ny;
}

void Sphere::getSurfaceCoordinate(
		AutoPtr<RayTask> ray_task, 
		real_t &x, real_t &y)
{
	// XXX TODO: north
	Vector inter = ray_task->getIntersection() - center;

	inter.unitize();

	y = acos(-inter.y) / M_PI;
	x = atan(inter.z / inter.x) / (M_PI * 2);
	if (inter.z * inter.x < 0)
		x += 0.5;
}

void Sphere::getCoordinateRange(
		real_t coord_low[3], real_t coord_high[3])
{
	coord_low[0] = center.x - radius;
	coord_low[1] = center.y - radius;
	coord_low[2] = center.z - radius;


	coord_high[0] = center.x + radius;
	coord_high[1] = center.y + radius;
	coord_high[2] = center.z + radius;
}

