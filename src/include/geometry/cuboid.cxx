/*
 * $File: cuboid.cxx
 * $Date: Fri Jun 08 00:20:44 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "cuboid.hxx"

void Cuboid::addRect(int v0, int v1, int v2, int v3)
{
	addFace(v0, v1, v2);
	addFace(v0, v2, v3);
}


Cuboid:: Cuboid(real_t a, real_t b, real_t c,
		const Vector &up,
		const Vector &left)
{
	// XXX TODO: rotation not implemented
	
	//Vector front = up.cross(left);
	//sassert(front.lengthsqr() > GEPS);

	real_t u = a * 0.5,
		   v = b * 0.5,
		   w = c * 0.5;

	addVertex(-u, -w, v);
	addVertex(u, -w, v);
	addVertex(u, -w, -v);
	addVertex(-u, -w, -v);

	addVertex(-u, w, v);
	addVertex(u, w, v);
	addVertex(u, w, -v);
	addVertex(-u, w, -v);

	addRect(0, 3, 2, 1);
	addRect(0, 1, 5, 4);
	addRect(1, 2, 6, 5);
	addRect(2, 3, 7, 6);
	addRect(3, 0, 4, 7);
	addRect(5, 6, 7, 4);

} 
