/*
 * $File: raytask.hxx
 * $Date: Mon Apr 30 11:56:36 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __RAYTASK_HEADER__
#define __RAYTASK_HEADER__


#include <prototype.hxx>

class GeometryPrimitive;
/**
 * RayTask is used when intersection exists
 * a geometry **MUST** define its corresponding
 * RayTask class
 */
class RayTask
{
	protected:

		Ray ray;

	public:

		RayTask(const Ray &ray);

		// return an unnormalized normal vector
		// with respect to the ray
		virtual Vector getNormal() = 0;

		// distance from start point to intersection
		virtual real_t getDist() = 0;

		virtual bool shootInsideGeometry() = 0;
		virtual bool shootOutsideGeometry() = 0;
		virtual GeometryPrimitive *getGeometry() = 0;
		virtual ~RayTask() {}


		Vector getIntersection();

};

#endif // __RAYTASK_HEADER__
