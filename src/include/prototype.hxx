/*
 * $File: prototype.hxx
 * $Date: Fri Jun 08 19:29:19 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __PROTOTYPE_HEADER__
#define __PROTOTYPE_HEADER__


//#include <math.hxx>
#include <color.hxx>
#include "setting.hxx"
#include <sassert.hxx>

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

class Vector2D
{
	public:
		real_t x, y;
		
		Vector2D(){}
		Vector2D(real_t x, real_t y)
			: x(x), y(y) {}

		Vector2D operator + (const Vector2D &vec) const
		{ return Vector2D(x + vec.x, y + vec.y); }

		Vector2D operator - (const Vector2D &vec) const
		{ return Vector2D(x - vec.x, y - vec.y); }

		Vector2D operator * (const real_t factor) const
		{ return Vector2D(x * factor, y * factor); }

		Vector2D operator / (const real_t factor) const
		{ return Vector2D(x / factor, y / factor); }

		Vector2D & operator += (const Vector2D &v) 
		{ x += v.x, y += v.y; return *this; }

		Vector2D & operator -= (const Vector2D &v) 
		{ x -= v.x, y -= v.y; return *this; }

		Vector2D & operator *= (const real_t factor)
		{ x *= factor, y *= factor; return *this; }

		Vector2D & operator /= (const real_t factor)
		{ x /= factor, y /= factor; return *this; }

		real_t dot(const Vector2D &v) const
		{ return x * v.x + y * v.y; }

		real_t cross(const Vector2D &v) const
		{ return x * v.y - y * v.x; }

		real_t length() const
		{ return sqrt(x * x + y * y); }

		real_t lengthsqr() const
		{ return x * x + y * y; }
};

/**
 * 3D vector
 */
class Vector
{
	public:
		real_t x, y, z;
		
		Vector(){}
		Vector(real_t x, real_t y, real_t z)
			: x(x), y(y), z(z) {}

		Vector operator + (const Vector &vec) const
		{ return Vector(x + vec.x, y + vec.y, z + vec.z); }

		Vector operator - (const Vector &vec) const
		{ return Vector(x - vec.x, y - vec.y, z - vec.z); }

		Vector operator * (const real_t factor) const
		{ return Vector(x * factor, y * factor, z * factor); }

		Vector operator / (const real_t factor) const
		{ return Vector(x / factor, y / factor, z / factor); }

		Vector & operator += (const Vector &v) 
		{ x += v.x, y += v.y, z += v.z; return *this; }

		Vector & operator -= (const Vector &v) 
		{ x -= v.x, y -= v.y, z -= v.z;  return *this; }

		Vector & operator *= (const real_t factor)
		{ x *= factor, y *= factor, z *= factor; return *this; }

		Vector & operator /= (const real_t factor)
		{ x /= factor, y /= factor, z /= factor; return *this; }

		inline real_t dot(const Vector &v) const
		{ return x * v.x + y * v.y + z * v.z; }

		Vector cross(const Vector &v) const
		{ return Vector(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x); }

		real_t length() const
		{ return sqrt(x * x + y * y + z * z); }

		real_t lengthsqr() const
		{ return x * x + y * y + z * z; }

		Vector operator - () const
		{ return (*this) * (-1); }

		Vector unit() const
		{ return (*this) / length(); }

		Vector &unitize()
		{ return (*this) /= length(); }

		Vector &tolength(real_t len)
		{
			return *this = unit() * len;
			//return unitize() *= len;
		}
};


Vector operator * (const real_t factor, const Vector &vec);
Vector operator / (const real_t factor, const Vector &vec);

/**
 * **Intensity** is the concept of `color` used in ray tracing
 * ranging in [0, 1]
 * while **Color** is used in image rendering
 */
class Intensity 
{
	private:
		void fixRange()
		{
			real_t ma = max();
			if (ma > 1)
				red /= ma, green /= ma, blue /= ma;
#ifdef __DEBUG_BUILD
			if (!(red >= 0 && green >= 0 && blue >= 0))
				sassert(0);
#endif
			return;
		}
	public:
		real_t red, green, blue;

		Intensity() {}
		Intensity(real_t red, real_t green, real_t blue)
			: red(red), green(green), blue(blue)
		{
			fixRange();
		}
		Intensity(real_t intensity)
			: red(intensity), green(intensity), blue(intensity)
		{
			fixRange();
		}

		Intensity(const Color &color) :
			red(color.redQuantum() / (real_t)MaxRGB),
			green(color.greenQuantum() / (real_t)MaxRGB),
			blue(color.blueQuantum() / (real_t)MaxRGB) {}


		Intensity(const char *color_name) :
			Intensity(Color(color_name))
		{
		}


		// numeric product
		Intensity operator * (const real_t factor) const
		{ return Intensity(red * factor, green * factor, blue * factor); }
		Intensity &operator *= (const real_t factor)
		{ return (*this) = (*this) * factor; }

		// component product
		Intensity operator * (const Intensity &intensity) const
		{ return Intensity(red * intensity.red, 
				green * intensity.green, 
				blue * intensity.blue); }

		Intensity operator + (const Intensity &intensity) const
		{ return Intensity(red + intensity.red,
				green + intensity.green,
				blue + intensity.blue); }
		Intensity &operator += (const Intensity &intensity) 
		{
			red += intensity.red;
			green += intensity.green;
			blue += intensity.blue;
			fixRange();
			return *this;
		}

		Intensity operator - (const Intensity &intensity) const
		{ return Intensity(red - intensity.red,
				green - intensity.green,
				blue - intensity.blue); }
		Intensity &operator -= (const Intensity &intensity)
		{
			red -= intensity.red;
			green -= intensity.green;
			blue -= intensity.blue;
			fixRange();
			return *this;
		}

		Intensity power(real_t p) const
		{ return Intensity(pow(red, p), pow(green, p), pow(red, p)); }

		// total intensity
		real_t intensity() const
		{ return 0.299 * red + 0.587 * green + 0.114 * blue; }

		real_t min() const
		{ return MIN(red, MIN(green, blue)); }
		real_t max() const
		{ return MAX(red, MAX(green ,blue)); }

};

inline Intensity operator * (real_t factor, const Intensity &intensity)
{ return Intensity(intensity.red * factor, 
		intensity.green * factor, 
		intensity.blue * factor); }

/**
 * `Ray` is the ray originally emited from view of point
 */
class Ray
{
	public:
		Ray(const Vector &start, const Vector &dir)
			: start(start), dir(dir)
		{}

		Ray(){}
		Vector start, dir; // direction vector
};

class LightSource
{
	public:
		Vector position;
		Intensity intensity;
		LightSource(const Vector &position, const Intensity &intensity)
			: position(position), intensity(intensity)
		{}
		LightSource(real_t x, real_t y, real_t z, 
				real_t red, real_t green, real_t blue)
			: position(x, y, z), intensity(red, green, blue)
		{}
		LightSource(real_t x, real_t y, real_t z, const Intensity &intensity)
			: position(x, y, z), intensity(intensity)
		{}
};

#endif // __PROTOTYPE_HEADER__
