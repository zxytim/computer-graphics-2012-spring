/*
 * $File: geometry.hxx
 * $Date: Sun Apr 29 11:09:42 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __GEOMETRY_HEADER__
#define __GEOMETRY_HEADER__

#include "geometry/geometry_primitive.hxx"
#include "geometry/sphere.hxx"
#include "geometry/plane.hxx"
#include "geometry/mesh.hxx"
#include "geometry/cuboid.hxx"

#endif // __GEOMETRY_HEADER__
