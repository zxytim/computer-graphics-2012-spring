/*
 * $File: prototype.cxx
 * $Date: Tue Apr 24 23:56:42 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "prototype.hxx"


Vector operator * (const real_t factor, const Vector &vec) 
{ return Vector(vec.x * factor, vec.y * factor, vec.z * factor); }

Vector operator / (const real_t factor, const Vector &vec)
{ return Vector(vec.x / factor, vec.y / factor, vec.z / factor); }
