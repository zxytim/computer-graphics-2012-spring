/*
 * $File: kdtree.hxx
 * $Date: Tue Jun 19 13:46:18 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_KDTREE__
#define __INCLUDE_KDTREE__

//#include "../renderable.hxx"

#include <algorithm>

#include <auto_ptr.hxx>
#include <global.hxx>
#include <timing.hxx>

#include <sassert.hxx>

#include <geometry/plane.hxx>

struct CoordRange
{
	real_t lo[3];
	real_t hi[3];

	void set(real_t lo[3], real_t hi[3])
	{
		memcpy(this->lo, lo, sizeof(real_t) * 3);
		memcpy(this->hi, hi, sizeof(real_t) * 3);
	}

	bool inside(const Vector &p, real_t eps) const
	{
		return p.x >= lo[0] - eps && p.y >= lo[1] - eps 
			&& p.z >= lo[2] - eps
			&& p.x <= hi[0] + eps && p.y <= hi[1] + eps
			&& p.z <= hi[2] + eps;
	}
};

/**
 * Axis-Aligned Plane
 */
struct AAPlane : public Plane
{
	int cid;
	real_t coord;
	void set(int cid_, real_t coord_)
	{
		cid = cid_, coord = coord_;
		real_t *bs = reinterpret_cast<real_t *>(&base);
		real_t *nml = reinterpret_cast<real_t *>(&normal);
		for (int i = 0; i < 3; i ++)
			bs[i] = nml[i] = 0;
		bs[cid] = coord;
		nml[cid] = 1;
	}
};

template<typename RenderablePrimitive, typename RayTask>
class KdTree
{
	public:

		const int SINGLE_NODE_OBJ_SIZE_THRESHOLD = 12;
		struct Intersection
		{
			real_t dist;
			RenderablePrimitive *obj;
			AutoPtr<RayTask> ray_task;
		};
	protected:

		struct Node
		{
			Node *ch[2];
			AAPlane plane;
			CoordRange crange;
			std::vector<int> obj;
			Node() {ch[0] = ch[1] = NULL;}
			bool isLeaf() const { return ch[0] == NULL && ch[1] == NULL; }
		};

		class RayDistUpperBoundExceedsCtrlFlow
		{
		};

		std::vector<RenderablePrimitive *> *rends;
		std::vector<CoordRange> crange;

		std::vector<int> non_dfnts;

		int dep_max;

		int nnode = 0;
		int nobjintree = 0;

		Node *root;

		int tree_dep_max = 0;
		// {{{ build tree
		Node *buildTree(std::vector<int> &ids, 
				real_t coord_low[3], real_t coord_high[3], int dep)
		{
			if (ids.size() == 0)
				return NULL;

			if (dep > tree_dep_max)
				tree_dep_max = dep;

			Node *root = new Node;
			root->crange.set(coord_low, coord_high);

			nnode ++;
			if (ids.size() <= (size_t)SINGLE_NODE_OBJ_SIZE_THRESHOLD || dep == dep_max)
			{
				root->obj = ids;
				nobjintree += ids.size();
				return root;
			}


			int coord_id = 0;
			int diff = ids.size() + 1;
			real_t sep_coord = REAL_INFINITY;
			for (int cid = 0; cid < 3; cid ++)
			{
#if 0
			// {{{ O_n
				real_t left = coord_low[cid],
					   right = coord_high[cid];
				int nl = 0, nr = 0, nm = 0;

				real_t mid = (left + right) * 0.5;
				for (auto i: ids)
				{
					if (crange[i].hi[cid] <= mid)
						nl ++;
					else if (crange[i].lo[cid] >= mid)
						nr ++;
					else
					{
						nm ++;
					}
				}
				int d = abs(nl - nr) + nm;
				if (d < diff)
				{
					coord_id = cid;
					diff = d;
					sep_coord = mid;
				}

				bool side = (nl <= nr);

				std::vector<int> active_id;
				for (auto i: ids)
					if (crange[i].hi[cid] <= mid)
					{
						if (side == 0)
							active_id.push_back(i);
					}
					else if (crange[i].lo[cid] >= mid)
					{
						if (side == 1)
							active_id.push_back(i);
					}
					else active_id.push_back(i);


				if (nl > nr) // left
					right = mid;
				else left = mid;


				real_t eps = (right - left) * 0.0001;
				while (right - left > eps && diff > 0)
				{
					real_t mid = (left + right) * 0.5;
					int l = 0, r = 0, m = 0;
					for (auto i: active_id)
					{
						if (crange[i].hi[cid] <= mid)
							l ++;
						else if (crange[i].lo[cid] >= mid)
							r ++;
						else
							m ++;
					}
					if (side == 0) // last: left
						r += nr;
					else l += nl;
					int d = abs(l - r) + m;
					if (d < diff)
					{
						coord_id = cid;
						diff = d;
						sep_coord = mid;
					}

					// update active_id
					// XXX: TODO use pointer instead
					//		to speed up
					std::vector<int> idtmp;

					for (auto i: active_id)
					{
						if (crange[i].hi[cid] <= mid)
						{
							if (side == 0)
								idtmp.push_back(i);
						}
						else if (crange[i].lo[cid] >= mid)
						{
							if (side == 1)
								idtmp.push_back(i);
						}
						else
						{
							idtmp.push_back(i);
						}
					}

					//assert(idtmp.size() == m + (side == 0 ? l : r));
					active_id = idtmp;

					side = (nl > nr);
					if (nl > nr)
						right = mid;
					else left = mid;
					nl = l, nr = r;
				}
				// }}} O_n
#elif 0
				// {{{binary_try
				real_t left = coord_low[cid],
					   right = coord_high[cid];
				double eps = 0.00001;
				while (right - left > eps && diff)
				{
					int nl = 0, nr = 0, nm = 0;
					double mid = (left + right) * 0.5;
					for (auto i: ids)
						if (crange[i].hi[cid] <= mid)
							nl ++;
						else if (crange[i].lo[cid] >= mid)
							nr ++;
						else nm ++;

					int d = abs(nl - nr) + nm;
					if (d < diff)
					{
						diff = d;
						sep_coord = mid;
						coord_id = cid;
					}

					if (nl > nr)
						right = mid;
					else left = mid;
				}
				// }}}
#elif 1
				// {{{ sort

				struct Event
				{
					int type, id;
					real_t pos;
				};

				std::vector<Event> event;
				for (auto i: ids)
				{
					Event e;
					e.type = 0;
					e.id = i;
					e.pos = crange[i].lo[cid];
					event.push_back(e);

					e.type = 1;
					e.id = i;
					e.pos = crange[i].hi[cid];
					event.push_back(e);

					assert(crange[i].lo[cid] <= crange[i].hi[cid]);
				}

				std::sort(event.begin(), event.end(),
						[](const Event &a, const Event &b)
						{ return a.pos < b.pos; });

				int nl = 0, nr = ids.size(), nm = 0;
				for (size_t i = 0; i < event.size();)
				{
					size_t j = i;
					while (fabs(event[j].pos - event[i].pos) < EPS)
					{
						if (event[j].type == 0)
							nm ++, nr --;
						else if (event[j].type == 1)
							nm --, nl ++;
						j ++;
					}
					int d = abs(nl - nr) * 2.5 + nm;
					if (d < diff)
					{
						diff = d;
						sep_coord = event[i].pos;
						coord_id = cid;
					}
					i = j;
				}
				// }}}
#elif 0
				/// TODO: more precise partition:
				//  NEED: judge whether a object share
				//		common part with a cuboid
#endif
			} 

#if 0 && defined(__DEBUG_BUILD)
			// check
			do
			{
				int l = 0, r = 0, m = 0;
				for (auto i: ids)
				{
					if (crange[i].hi[coord_id] <= sep_coord)
						l ++;
					else if (crange[i].lo[coord_id] >= sep_coord)
						r ++;
					else
						m ++;
				}
				int v = abs(l - r) + m;
				assert(v == diff);
			} while (0);
#endif

			int DIFF_THRESHOLD = MAX(12.0, ids.size() * 0.14);
			//int DIFF_THRESHOLD = MAX(5.0, ids.size() * 0.10);
			if (diff <= DIFF_THRESHOLD && !(diff == (int)ids.size())) // deep in
			{
				root->plane.set(coord_id, sep_coord);

				std::vector<int> lids, rids;
				for (auto i: ids)
					if (crange[i].hi[coord_id] <= sep_coord)
						lids.push_back(i);
					else if (crange[i].lo[coord_id] >= sep_coord)
						rids.push_back(i);
					else lids.push_back(i), rids.push_back(i);

				real_t t = coord_high[coord_id];
				coord_high[coord_id] = sep_coord;
				root->ch[0] = buildTree(lids, coord_low, coord_high, dep +1);
				coord_high[coord_id] = t;

				t = coord_low[coord_id];
				coord_low[coord_id] = sep_coord;
				root->ch[1] = buildTree(rids, coord_low, coord_high, dep + 1);
				coord_low[coord_id] = t;

				return root;
			}
			else // leaf
			{
				//printf("size:%d, diff:%d, DIFF_THRESHOLD:%d\n", ids.size(), diff, DIFF_THRESHOLD);
				nobjintree += ids.size();
				root->obj = ids;
				return root;
			}
			assert(0);
			return NULL;
		}
		// }}} build tree

		// {{{ misc
		static void update(real_t a[3], real_t b[3], bool (*cmp)(real_t, real_t))
		{
			for (int i = 0; i < 3; i ++)
				if (cmp(a[i], b[i]))
					a[i] = b[i];
		}

		void updateByDist(Intersection &a,
				Intersection &b)
		{
			if (b.dist < a.dist)
				a = b;
		}

		void release(Node *root)
		{
			if (!root)
				return;
			release(root->ch[0]);
			release(root->ch[1]);
			delete root;
			return;
		}

		// }}} misc

		// {{{ moveStartPointInside
		/**
		 * move start point of ray inside the range.
		 * if can not intersect, return false
		 */
		static bool moveStartPointInside(
				const CoordRange &crange, const Ray &ray_orig,
				Vector &start_point, real_t &ray_len)
		{
			AAPlane planes[6];
			for (int i = 0; i < 3; i ++)
			{
				planes[i * 2].set(i, crange.lo[i]);
				planes[i * 2 + 1].set(i, crange.hi[i]);
			}

			real_t dist = REAL_INFINITY + 1,
				   dist2 = REAL_INFINITY + 2;



#ifdef __DEBUG_BUILD
			static int cnt = 0;
			cnt ++;

			Vector inter1, inter2;
			decltype(planes[0].intersectionTest(ray_orig)) 
				rt1, rt2;
#endif

			for (int i = 0; i < 6; i ++)
			{
				auto ray_task = planes[i].intersectionTest(ray_orig);
				if (ray_task.ptr() != NULL)
				{
					Vector inter = ray_task->getIntersection();

					if (!crange.inside(inter, EPS))
						continue;

					real_t d = ray_task->getDist();
					if (d < dist)
					{
						dist2 = dist;
						dist = d;
#ifdef __DEBUG_BUILD

						inter2 = inter1;
						inter1 = inter;

						rt2 = rt1;
						rt1 = ray_task;
#endif
					}
					else if (d < dist2)
					{
						dist2 = d;
#ifdef __DEBUG_BUILD
						inter2 = inter, rt2 = ray_task;
#endif
					}
				}
			}

			if (dist >= REAL_INFINITY) // not intersected
				return false;

			start_point = ray_orig.start;
			if (!crange.inside(start_point, EPS))
			{
				start_point = ray_orig.start + ray_orig.dir * dist;
				while (!crange.inside(start_point, EPS))
					start_point += ray_orig.dir * EPS * 0.5;

				if (dist2 >= REAL_INFINITY)
					dist2 = dist;

				//sassert(dist2 < REAL_INFINITY);
				ray_len = dist2;
			}
			else
				ray_len = dist;

			return true;
		}
		// }}} moveStartPointInside

		// {{{ getIntersection in tree
		Intersection getIntersection(Node *root, 
				Vector start_point, real_t ray_len, const Ray &ray_orig, real_t dist_lower_bound)
		{
			real_t ray_len_lower = (start_point - ray_orig.start).length();

			assert(root->crange.inside(start_point, EPS));

			Intersection nul_inter;
			nul_inter.dist = REAL_INFINITY + 1;
			nul_inter.obj = NULL;

			if (root == NULL)
				return nul_inter;

			if (root->isLeaf())
			{
				Intersection inter;
				inter.dist = REAL_INFINITY + 1;
				inter.obj = NULL;

				AutoPtr<RayTask> ray_task;
				for (auto i: root->obj)
				{
					// HASH
					ray_task = (*rends)[i]->intersectionTest(ray_orig);

					if (!ray_task)
						continue;
					real_t dist = ray_task->getDist();

					if (dist < dist_lower_bound - EPS)
						throw RayDistUpperBoundExceedsCtrlFlow();

					sassert(dist > 0);

					// IMPORTANT
					if (dist > ray_len + EPS)
						continue;

					// IIMMPPOORRTTAANNTT!!!
					if (dist + EPS < ray_len_lower)
						continue;

					if (dist < inter.dist)
					{
						inter.dist = dist;
						inter.ray_task = ray_task;
						inter.obj = (*rends)[i];
					}
				}

				return inter;
			}

			// not leaf

			do
			{
				Vector new_spoint[2];
				real_t ray_len[2];
				Node *ch[2];
				int cnt = 0;
				for (int i = 0; i < 2; i ++)
					if (root->ch[i] && moveStartPointInside(root->ch[i]->crange, ray_orig, 
								new_spoint[cnt], ray_len[cnt]))
						ch[cnt ++] = root->ch[i];
				if (cnt == 1)
					return getIntersection(ch[0], new_spoint[0], ray_len[0], ray_orig, dist_lower_bound);
				else if (cnt == 2)
				{
					if (!ch[0]->crange.inside(start_point, EPS))
					{
						std::swap(ch[0], ch[1]);
						std::swap(ray_len[0], ray_len[1]);
						std::swap(new_spoint[0], new_spoint[1]);
					}
					Intersection inter = 
						getIntersection(ch[0], new_spoint[0], ray_len[0], ray_orig, dist_lower_bound);

#ifndef __DEBUG_BUILD
					if (inter.obj != NULL)
						return inter;
					return getIntersection(ch[1], new_spoint[1], ray_len[1], ray_orig, dist_lower_bound);
#else
					Intersection inter2 = getIntersection(ch[1], new_spoint[1], ray_len[1], ray_orig, dist_lower_bound);


					if (inter.obj != NULL)
					{
						real_t d1 = inter.ray_task->getDist();
						if (inter2.obj != NULL)
						{
							real_t d2 = inter2.ray_task->getDist();
							sassert(d1 <= d2 + EPS);
							if (d1 > d2)
								return inter2;
							return inter;
						}
						return inter;
					}

					return inter2;
#endif
				}
				else
				{
					sassert(root->ch[0] == NULL || root->ch[1] == NULL);
					return nul_inter;
				}
			} while (0);
			sassert(0);
			return nul_inter;

		}
		// }}} getIntersection in tree

		// {{{ debug
		bool findFace(Node *root, int fid)
		{
			if (root == NULL)
				return false;
			if (root->isLeaf())
			{
				for (auto i: root->obj)
					if (i == fid)
						return true;
				return false;
			}

			for (int i = 0; i < 2; i ++)
				if (findFace(root->ch[i], fid))
					return true;
			return false;
		}

		// }}} debug


	public:

		KdTree()
		{
			root = NULL;
		}
		~KdTree()
		{
			release(root);
		}

		// {{{ init tree
		void init(std::vector<RenderablePrimitive *> &renderables)
		{
			TIMING_START();

			rends = &renderables;
			std::vector<int> ids;
			crange.resize(rends->size());

			real_t coord_low[3],
				   coord_high[3];
			for(int i = 0; i < 3; i ++)
				coord_low[i] = REAL_INFINITY + 1, coord_high[i] = -REAL_INFINITY - 1;

			for (size_t i = 0; i < rends->size(); i ++)
				if ((*rends)[i]->isDefinite())
				{
					real_t lo[3], hi[3];
					(*rends)[i]->getCoordinateRange(lo, hi);
					crange[i].set(lo, hi);
					update(coord_low, lo,
							[](real_t a, real_t b){return b < a;});
					update(coord_high, hi,
							[](real_t a, real_t b){return b > a;});
					ids.push_back(i);
				}
				else non_dfnts.push_back(i);

#ifdef __DEBUG_BUILD
			for (int i = 0; i < 3; i ++)
			{
				assert(coord_low[i] > -REAL_INFINITY);
				assert(coord_high[i] < REAL_INFINITY);
			}
#endif

			dep_max = MAX(5, (int)floor(log((real_t)ids.size()) / log(2.0) + 2));

			root = buildTree(ids, coord_low, coord_high, 0);

			fprintf(stderr, "---- kdtree info ---- \n");
			fprintf(stderr, "nodes: %d\n", nnode);
			fprintf(stderr, "objs: %lu\n", rends->size());
			fprintf(stderr, "nobjintree: %d\n", nobjintree);
			fprintf(stderr, "dep/dep_max: %d/%d\n", tree_dep_max, dep_max);
			fprintf(stderr, "---- end info ---- \n");


#ifdef __DEBUG_BUILD
			findFace(root, 0);
#endif

			TIMING_END("build kd-tree");
		}
		// }}} init tree

		// {{{ getIntersection
		Intersection getIntersection(Ray ray, real_t dist_lower_bound = -REAL_INFINITY - 10)
		{
			ray.dir.unitize();

			Intersection inter;
			inter.dist = REAL_INFINITY + 1;
			inter.obj = NULL;


			real_t ray_len = -1;
			Vector start_point;

			if (root != NULL)
			{
				if (moveStartPointInside(root->crange, ray, start_point, ray_len))
					inter = getIntersection(root, start_point, ray_len, ray, dist_lower_bound);

			}

			for (auto i: non_dfnts)
			{
				Intersection inter_tmp;
				auto ray_task = (*rends)[i]->intersectionTest(ray);
				if (ray_task.ptr() == NULL)
					continue;
				real_t dist = (ray_task->getIntersection() - ray.start).length();

				if (dist > dist_lower_bound)
					throw RayDistUpperBoundExceedsCtrlFlow();

				if (dist < inter.dist)
				{
					inter.dist = dist;
					inter.obj = (*rends)[i];
					inter.ray_task = ray_task;
				}
			}

			return inter;
		}

		bool lightShaded(Ray ray, real_t dist_lower_bound)
		{
			try
			{
				getIntersection(ray, dist_lower_bound);
			}
			catch (RayDistUpperBoundExceedsCtrlFlow)
			{
				return true;
			}
			return false;
		}

		// }}} getIntersection

};

#endif // __INCLUDE_KDTREE__

/**
 * vim: fdm=marker
 */
