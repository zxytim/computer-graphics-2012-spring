/*
 * $File: renderable.cxx
 * $Date: Sun Apr 29 23:00:20 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "renderable.hxx"

#include <sassert.hxx>

RenderablePrimitive::RenderablePrimitive(
		AutoPtr<GeometryPrimitive> geometry,
		AutoPtr<Material> material,
		string name,
		AutoPtr<BDObjectPrimitive> bdobject) :
	geometry(geometry), material(material),
	bdobject(bdobject),
	m_name(name)
{
}

AutoPtr<RayTask> RenderablePrimitive::intersectionTest(
		const Ray &ray)
{
	sassert(fabs(ray.dir.lengthsqr() - 1) < EPS);

	if (!bdobject->BDIntersectionTest(ray))
		return NULL;
	return geometry->intersectionTest(ray);
}

AutoPtr<SurfaceProperty> RenderablePrimitive::getSurfaceProperty(
		AutoPtr<RayTask> &ray_task)
{
	real_t x, y;
	geometry->getSurfaceCoordinate(ray_task, x, y);
	auto &texture = material->texture;
	return texture->getSurfaceProperty(x, y);
}


const string &RenderablePrimitive::name()
{
	return m_name;
}

