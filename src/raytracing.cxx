/*
 * $File: raytracing.cxx
 * $Date: Sat Jun 09 15:36:23 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include <algorithm>
#include <pthread.h>
#include <unistd.h>
#include <cstdlib>
#include <vector>

#include "raytracing.hxx"
#include <kdtree.hxx>

RayTracing::RayTracing()
	: trace_depth_max(4), 
	min_trace_energy(0.02),
	nthread(1),
	enable_kdtree(-1)
{
}

RayTracing::~RayTracing()
{
}

void RayTracing::setTraceDepth(int depth)
{
	trace_depth_max = depth;
}


void RayTracing::setMinTraceEnergy(real_t energy)
{
	min_trace_energy = energy;
}

void RayTracing::setThreadNumber(int nthread)
{
	sassert(nthread > 0);
	this->nthread = nthread;
}

void RayTracing::setKdTree(int status)
{
	this->enable_kdtree = status;
}


static int max_depth;
#include <ctime>
#include <sys/time.h>

namespace RayTracingImpl
{
	struct ThreadArg
	{
		RayTracing *instance;
		bool show_log;
	};

	ThreadArg args[1000];

	void *rayTracingThreadProxy(void *arg)
	{
		ThreadArg *targ = static_cast<ThreadArg *>(arg);
		sassert(targ->instance->scene->frustum.dist > EPS);
		targ->instance->rayTracingThread(*(targ->instance->scene), targ->show_log);
		return NULL;
	}

	const double NUM_PER_SEC = 1000000;
	static long long getTime()
	{
		timeval tv;
		gettimeofday(&tv, 0);
		return tv.tv_sec * 1000000 + tv.tv_usec;
	}
}

using namespace RayTracingImpl;

void RayTracing::rayTracingThread(Scene &scene, bool show_log)
{
	const Scene::Frustum &frustum = scene.frustum;
	Vector dist = scene.view_dir * frustum.dist;
	Vector origin = scene.view_pos + dist;
	Vector right_dir = dist.unit().cross(scene.up_dir);

	assert(fabs(right_dir.length() - 1) < EPS);

	real_t x_unit = (frustum.right - frustum.left) / scene.width(),
		   y_unit = (frustum.top - frustum.bottom) / scene.height();

	max_depth = 0;

	int cnt = 0;
	for (; ; )
	{

		nrendered_pixel ++;
		cnt ++;

		// start lock
		pthread_mutex_lock(&render_mutex);

		if (render_task_queue.size() == 0)
		{
			pthread_mutex_unlock(&render_mutex);
			break;
		}
		RenderPixelTask task = render_task_queue.front();
		render_task_queue.pop();

		pthread_mutex_unlock(&render_mutex);
		// end lock


		if (show_log)
		{
			int percent =  (nrendered_pixel)/ (double)rpixel_total * 10000.0;
			if (percent != last_percent)
			{
#if 1
				double elapse = (getTime() - start_time) / NUM_PER_SEC;
				fprintf(stderr, "[%.2lf%%] %3.2lfs, eta %3.2lfs, rendering ...\r", 
						percent / 100.0, 
						elapse,
						elapse / (percent / 10000.0) - elapse
						);
#else
				fprintf(stderr, "[%.2lf%%] rendering ...\r", percent / 100.0);
#endif
				last_percent = percent;
			}
		}

		double x = frustum.left + x_unit * task.x,
			   y = frustum.bottom + y_unit * task.y;

		Vector point_on_frustum(origin + right_dir * x + scene.up_dir * y);
		Vector sight_dir = (point_on_frustum - scene.view_pos).unit();

		Ray start_ray(scene.view_pos, sight_dir);

		// XXX: assume start ray is 
		// outside every object
		Intensity intensity = rayTracing(
				start_ray, REFRACTIVE_INDEX_AIR,
				1.0, DENSITY_AIR, 0, 0, scene);

		*(task.result) = intensity;
		//image_buffer[i * height + j] = intensity;
		//scene.image_renderer.pixelColor(i, j, color);
	}
	if (show_log)
	{
		fprintf(stderr, "\nthread rendered pixels: %d\n", cnt);
	}
}

void RayTracing::create_tracing_threads(int nthread, bool show_log, pthread_t *thread)
{
	for (int tid = 0; tid < nthread; tid ++)
	{
		RayTracingImpl::ThreadArg &arg = RayTracingImpl::args[tid];
		arg.instance = this;

		sassert(arg.instance->scene->frustum.dist > EPS);

		arg.show_log = show_log;

		//fprintf(stderr, "creating thread %u: ...\n", tid);

		pthread_t id;

		pthread_create(&id, NULL, RayTracingImpl::rayTracingThreadProxy, &arg);
		thread[tid] = id;
	}
}

void RayTracing::wait_tracing_threads(int nthread, pthread_t *thread)
{
	fprintf(stderr, "%d threads working ...\n", nthread);
	for (int i = 0; i < nthread; i ++)
		pthread_join(thread[i], NULL);
	fprintf(stderr, "\n");
}

void RayTracing::render(Scene &scene)
{

	fprintf(stderr, "max trace depth: %d\n", trace_depth_max);
	fprintf(stderr, "medium density: %s\n", scene.enable_medium_density ? "on" : "off");
	fprintf(stderr, "number of thread: %d\n", nthread);
	fprintf(stderr, "shadow: %s\n", scene.enable_shadow ? "on" : "off");
	fprintf(stderr, "Anti-Aliasing: %s\n", scene.anti_aliasing ? (
				scene.anti_aliasing == AA_CFAA ? "CFAA" : "FSAA"
				) : "NONE");

	this->scene = &scene;

	if (enable_kdtree == -1)
	{
		if (scene.renderable.size() <= 20)
			enable_kdtree = false;
		else enable_kdtree = true;
	}

	if (enable_kdtree)
	{
		fprintf(stderr, "KdTree enabled.\n");
#ifdef __DEBUG_TEST_KD_TREE
		fprintf(stderr, "[WARNING] Testing KdTree, this may cause **VERY LOW** efficiency\n");
#endif
		kdtree.init(scene.renderable);
	}

	int width = scene.width();
	int height = scene.height();

	npixels = width * height;

	image_buffer = new Intensity[npixels];

	pthread_mutex_init(&render_mutex, NULL);

	// add task
	for (int i = 0; i < width; i ++)
		for (int j = 0; j < height; j ++)
		{
			RenderPixelTask task;
			task.x = i + 0.5;
			task.y = j + 0.5;
			task.result = image_buffer + (i * height + j);
			render_task_queue.push(task);
		}

	// pthread
	rpixel_total = render_task_queue.size();
	nrendered_pixel = 0;

	start_time = getTime();

	fprintf(stderr, "\n");

	TIMING_START();

	fprintf(stderr, "pixels: %dx%d=%d\n", width, height, width * height);

	pthread_t thread0[nthread];
	create_tracing_threads(nthread, true, thread0);
	wait_tracing_threads(nthread, thread0);

	TIMING_END("Render procedure");

	start_time = getTime();
	// Anti Aliasing
	// TODO 4x 8x
	if (scene.anti_aliasing)
	{
		// output the original file
		for (int i = 0; i < width; i ++)
			for (int j = 0; j < height; j ++)
			{
				int id = i * height + j;
				scene.image_renderer.pixelColor(
						i, j, Color(image_buffer[id].red, 
							image_buffer[id].green, 
							image_buffer[id].blue));
			}
		const char *orig_name = "original.png";
		fprintf(stderr, "original un-anti-aliased picture is saving to `%s`\n", orig_name);
		scene.write(orig_name);


		pthread_t thread_aa[1 + 6 + 18];

		Intensity *aa_buffer = new Intensity[npixels];
		//	usleep(10000);

		fprintf(stderr, "Anti-aliasing Type %s \n",
				scene.anti_aliasing == AA_FSAA ? "FSAA" :
				(
				 scene.anti_aliasing == AA_CFAA ? "CFAA" :
				 "Unkown"
				)
			   );

		static int dir_x[8] = {1, 1, 0, -1, -1, -1, 0, 1};
		static int dir_y[8] = {0, 1, 1, 1, 0, -1, -1, -1};
		static real_t value_table[8] = {
			2, 1, 2, 1, 2, 1, 2, 1
		};
		for (int i = 0; i < 8; i ++)
			value_table[i] /= 16;

		real_t aa_phase = 1000,
			   aa_threshold = sqr(aa_phase * scene.aa_cfaa_coef);

		real_t cfaa_threshold = aa_threshold;

		if (scene.anti_aliasing == AA_CFAA)
			fprintf(stderr, "CFAA coef: %lf\n", scene.aa_cfaa_coef);
		else if (scene.anti_aliasing == AA_FSAA)
			aa_threshold = 0;

		std::queue<int> aa_pos;

		std::queue<int> cfaa_pos;

		bool *hash = new bool[npixels * 2 * 2]; // 2x
		memset(hash, 0, sizeof(bool) * npixels * 2 * 2);

		Intensity *aa_map = new Intensity[npixels * 2 * 2];

		TIMING_START();

		int last_percent = -1;
		for (int i = 0; i < width; i ++)
			for (int j = 0; j < height; j ++)
			{
				int pos = i * height + j;


				// log
				int percent = pos / (double) npixels * 10000;
				if (percent != last_percent)
				{
					fprintf(stderr, "[%.2lf%%]generating anti-aliasing pixels...\r", 
							percent / 100.0);
					last_percent = percent;
				}

				real_t val = 0;
				Intensity &cur_int = image_buffer[pos];
				for (int k = 0; k < 8; k ++)
				{
					int x = i + dir_x[k],
						y = j + dir_y[k];
					if (!(x >= 0 && x < width && y >= 0 && y < height))
						continue;
					Intensity &buf = image_buffer[x * height + y];

					real_t v = sqr(cur_int.red - buf.red)
						+ sqr(cur_int.green - buf.green)
						+ sqr(cur_int.blue - buf.blue);
					val += v * sqr(aa_phase)/ 3;
				}

				if (val >= aa_threshold)
				{
					aa_pos.push(pos);

					if (scene.anti_aliasing == AA_FSAA)
					{
						if (val >= cfaa_threshold)
							cfaa_pos.push(pos);
					}

					real_t cx = i + 0.5,
						   cy = j + 0.5;
					// XXX: only 2x
					for (int k = 0; k < 8; k ++)
					{
						// 2x large map
						int x = i * 2 + dir_x[k],
							y = j * 2 + dir_y[k];
						if (!(x >= 0 && x < width * 2&& y >= 0 && y < height * 2))
							continue;

						int p = x * height * 2 + y;

						if (!hash[p])
						{
							hash[p] = true;
							RenderPixelTask task;
							task.x = cx + dir_x[k] * 0.5;
							task.y = cy + dir_y[k] * 0.5;
							task.result = aa_map + (x * height * 2 + y);
							render_task_queue.push(task);
						}
					}
				}
				else aa_buffer[pos] = cur_int;
			}

		fprintf(stderr, "\n");
		fprintf(stderr, "pixels %d -> %.2lf%%\nadditional pixels: %d -> %.2lf%%\n", 
				(int)aa_pos.size(), aa_pos.size() / (double)npixels * 100,
				(int)render_task_queue.size(), render_task_queue.size()
				/ (double)npixels * 100);

		delete [] hash;

		rpixel_total = render_task_queue.size();
		nrendered_pixel = 0;
		// Running tasks
		create_tracing_threads(nthread, true, thread_aa);
		wait_tracing_threads(nthread, thread_aa);

		TIMING_END("Anti aliasing");

		if (scene.anti_aliasing == AA_FSAA)
		{
			fprintf(stderr, "CFAA samples: %d\n", (int)cfaa_pos.size());
			memcpy(aa_buffer, image_buffer, sizeof(Intensity) * npixels);
			while (cfaa_pos.size())
			{
				int pos = cfaa_pos.front();
				cfaa_pos.pop();

				Intensity &new_int = aa_buffer[pos];
				new_int = image_buffer[pos] * 0.25;
				int i = pos / height,
					j = pos % height;

				for (int k = 0; k < 8; k ++)
				{
					// 2x large map
					int x = i * 2 + dir_x[k],
						y = j * 2 + dir_y[k];
					if (!(x >= 0 && x < width * 2&& y >= 0 && y < height * 2))
						continue;

					int p = x * height * 2 + y;

					new_int += value_table[k] * aa_map[p];
				}
			}


			fprintf(stderr, "dealing with cfaa picture ...\n");
			// output the CFAA picture
			for (int i = 0; i < width; i ++)
				for (int j = 0; j < height; j ++)
				{
					int id = i * height + j;
					scene.image_renderer.pixelColor(
							i, j, Color(aa_buffer[id].red, 
								aa_buffer[id].green, 
								aa_buffer[id].blue));
				}

			const char *cfaa_name = "cfaa.png";
			fprintf(stderr, "cfaa picture is saving to `%s`\n", cfaa_name);
			scene.write(cfaa_name);

			fprintf(stderr, "cfaa picture saved ...\n");
		}

		if (scene.anti_aliasing == AA_FSAA)
		{
			fprintf(stderr, "dealing with fsaa HD picture ...\n");
			fprintf(stderr, "reseting image resolution ... ");
			scene.setImageResolution(width * 2, height * 2);
			fprintf(stderr, "done\n");

			fprintf(stderr, "imaging ...\n");
			// output the 4 times larger picture
			for (int i = 0; i < width * 2; i ++)
				for (int j = 0; j < height * 2; j ++)
				{
					int id = i * height * 2 + j;
					scene.image_renderer.pixelColor(
							i, j, Color(aa_map[id].red, 
								aa_map[id].green, 
								aa_map[id].blue));
				}
			for (int i = 0; i < width; i ++)
				for (int j = 0; j < height; j ++)
				{
					Intensity t = image_buffer[i * height + j];
					scene.image_renderer.pixelColor(
							i * 2, j * 2, Color(t.red, t.green, t.blue));
				}

			const char *fsaa_HD_name = "fsaa-used-2x.png";
			fprintf(stderr, "fsaa used HD picture is saving to `%s`\n", fsaa_HD_name);
			fprintf(stderr, "writing ... ");
			scene.write(fsaa_HD_name);
			fprintf(stderr, "done\n");

			fprintf(stderr, "set image resolution back ... ");
			scene.setImageResolution(width, height);
			fprintf(stderr, "done\n");
		}

		// probobly FSAA
		fprintf(stderr, "blending ...\n");
		while (aa_pos.size())
		{
			int pos = aa_pos.front();
			aa_pos.pop();

			Intensity &new_int = aa_buffer[pos];
			new_int = image_buffer[pos] * 0.25;
			int i = pos / height,
				j = pos % height;

			for (int k = 0; k < 8; k ++)
			{
				// 2x large map
				int x = i * 2 + dir_x[k],
					y = j * 2 + dir_y[k];
				if (!(x >= 0 && x < width * 2&& y >= 0 && y < height * 2))
					continue;

				int p = x * height * 2 + y;

				new_int += value_table[k] * aa_map[p];
			}
		}

		memcpy(image_buffer, aa_buffer, sizeof(Intensity) * npixels);
		delete [] aa_map;
		delete [] aa_buffer;
	}

	fprintf(stderr, "final imgaing...\n");
	for (int i = 0; i < width; i ++)
		for (int j = 0; j < height; j ++)
		{
			int id = i * height + j;
			scene.image_renderer.pixelColor(
					i, j, Color(image_buffer[id].red, 
						image_buffer[id].green, 
						image_buffer[id].blue));
		}

	delete [] image_buffer;

	fprintf(stderr, "max traced depth: %d\n", max_depth);
}


Intensity RayTracing::rayTracing(Ray & ray,
		real_t refractive_index, Intensity energy,
		real_t medium_density,
		int ninside, 
		int depth, Scene & scene)
{
	//scene.renderable[0];

	// XXX: dirty little hack
	ray.start += ray.dir * EPS;

	if (depth == trace_depth_max)
		return Intensity(0, 0, 0);

	if (depth > max_depth)
		max_depth = depth;

	KdTree<RenderablePrimitive, RayTask>::Intersection inter;
	inter.dist = REAL_INFINITY;
	inter.obj = NULL;

	//Intersection inter2 = inter;

#ifdef __DEBUG_TEST_KD_TREE
	KdTree<RenderablePrimitive, RayTask>::Intersection kdtreeinter;
#endif
	if (enable_kdtree)
	{
#ifdef __DEBUG_TEST_KD_TREE
		kdtreeinter 
#else
		inter 
#endif
		= kdtree.getIntersection(ray);
	}
#ifndef __DEBUG_TEST_KD_TREE
	else
#endif
	{
		inter.dist = REAL_INFINITY;
		inter.obj = NULL;
		//inter.ray_task = NULL;
		// INTERTEST
		for (size_t i = 0; i < scene.renderable.size(); i ++)
			//for (auto obj : scene.renderable)
		{
			RenderablePrimitive *obj =
				scene.renderable[i];

			AutoPtr<RayTask> ray_task = obj->intersectionTest(ray);

			if (ray_task.ptr())
				// intersects
			{
				real_t dist = ray_task->getDist();

				// NOTE: not EPS because sometimes
				// it could be very small
				sassert(dist > 0);
				if (dist < inter.dist)
				{
					//	inter2 = inter;
					inter.dist = dist;
					inter.ray_task = ray_task;
					inter.obj = obj;
				}
				/*
				   else if (dist < inter2.dist)
				   {
				   inter2.dist = dist;
				   inter2.ray_task = ray_task;
				   inter2.obj = obj;
				   }
				   */
			}
		}
	}

#if __DEBUG_TEST_KD_TREE
	if (enable_kdtree)
	{
		assert(inter.obj == kdtreeinter.obj);
		if (inter.obj != NULL)
		{
			real_t dinter = inter.ray_task->getDist();
			real_t dkdtreeinter = kdtreeinter.ray_task->getDist();
			assert(fabs(dinter - dkdtreeinter) < EPS);
			Vector a = inter.ray_task->getIntersection();
			Vector b = inter.ray_task->getIntersection();
			assert(fabs((a - b).lengthsqr() < EPS));
			if (fabs(dinter - dkdtreeinter) >= EPS)
			{
				Vector a = inter.ray_task->getIntersection();
				Vector b = inter.ray_task->getIntersection();
				assert(0);
				int asdf = 0;
			}
		}
		else assert(kdtreeinter.obj == NULL);
	}
#endif

	if (inter.obj == NULL)
		return Intensity(0, 0, 0);

	real_t dist = inter.dist;
	RenderablePrimitive *obj = inter.obj;
	AutoPtr<RayTask> ray_task = inter.ray_task;

	Vector intersection = ray.start + ray.dir * dist,
		   normal = ray_task->getNormal().unit();

	// XXX
	sassert(normal.lengthsqr() > EPS);


	AutoPtr<SurfaceProperty> surface = obj->getSurfaceProperty(ray_task);
	Intensity texture = surface->base_intensity;

	// Phong shading model
	//		light intensity = ambient + light source
	Intensity light_intensity = scene.ambient * texture * surface->blend.ambient;

	// diffuse && specular
	for (const auto &light : scene.light_source)
	{
		Ray light_ray(light.position, (intersection - light.position).unit());

		// TODO: Speed up
		if (scene.enable_shadow)
		{
			try
			{
				real_t d0 = (intersection - light.position).length();

#ifdef __DEBUG_BUILD
				static int cnt = 0;
				cnt ++;
				if (cnt == 653)
					int asdf = 0;
#endif
				int ans_kdtree = 0, ans_force = 0;
				if (enable_kdtree)
				{
					if (kdtree.lightShaded(light_ray, d0))
#ifdef __DEBUG_TEST_KD_TREE
						ans_kdtree = 1;
#else
						throw 1;
#endif
				}
#ifndef __DEBUG_TEST_KD_TREE
				else
#endif
				{
					// INTERTEST
					for (size_t i = 0; i < scene.renderable.size(); i ++)
						//for (auto other : scene.renderable)
					{
						RenderablePrimitive* other = 
							scene.renderable[i];
						// light ray rask
						AutoPtr<RayTask> lrtask = other->intersectionTest(light_ray);
						if (!lrtask.ptr())
							continue;

						// light is shaded by another object
						if (lrtask->getDist() < d0 - EPS)
#ifdef __DEBUG_TEST_KD_TREE
							ans_force = 1;
#else
							throw 1;
#endif
					}
				}
#ifdef __DEBUG_TEST_KD_TREE
				if (enable_kdtree)
				{
					assert(ans_kdtree == ans_force);
					if (ans_force)
						throw ans_force;
					//if (ans_kdtree)
					//	throw ans_kdtree;
				}
#endif
			}
			catch(int )
			{
				// shaded
				continue;
			}
		}

		// not shaded, using Phong reflection model

		Vector light_reflect = getReflect(light_ray.dir, normal);

		sassert(fabs(light_reflect.lengthsqr() - 1) < EPS);

		Intensity diffuse(0, 0, 0),
				  specular(0, 0, 0);

		real_t dot_val = normal.dot(-light_ray.dir);
		if (dot_val > EPS)
			diffuse = texture * light.intensity * surface->blend.diffuse * dot_val;

		real_t pow_base = -light_reflect.dot(ray.dir);
		if (pow_base > EPS)
			specular = light.intensity * surface->blend.specular
				* pow(pow_base, obj->material->shininess);

		// NOTE: class Intensity dim the light automatically
		light_intensity += diffuse + specular;
	}



	// TODO:
	// Beer's Law
	// The energy losses while passing through
	// a medium. the law tells (simplified):
	//		energy_end = energy_start * e^(-(dist * density))
	// 

	if (scene.enable_medium_density)
		energy *= exp(-dist * medium_density);


	light_intensity = light_intensity * energy;

	if (depth == trace_depth_max - 1)
		return light_intensity;

	// deepening

	Intensity reflect_intensity(0, 0, 0);

	// REFLECTION
	do
	{
		Intensity reflect_energy = energy * surface->energy.reflect;
		if (reflect_energy.max() < min_trace_energy)
			break;

		Ray reflect_ray(intersection, getReflect(ray.dir, normal));

		// refractive index and medium density 
		// is not changed due to the same medium 

		reflect_intensity = 
			rayTracing(reflect_ray, refractive_index,
					reflect_energy, medium_density,
					ninside, depth + 1, scene);
	} while (0);

	Intensity refract_intensity(0, 0, 0);
	// REFRACTION
	do
	{
		Intensity refract_energy = energy * surface->energy.refract;

		if (refract_energy.max() < min_trace_energy)
			break;

		real_t refract_density;
		real_t refract_rindex;

#if 1

		RenderablePrimitive *rend_in(NULL);

		if (ray_task->shootInsideGeometry())
		{
			ninside ++;
			//sassert(fabs(refractive_index - REFRACTIVE_INDEX_AIR) < EPS);
			rend_in = obj;
		}
		else
		{
			// NOTE: some geometry does not have a closed surface
			if (ray_task->shootOutsideGeometry())
				ninside --;
			if (ninside != 0)
				rend_in = inter.obj; //inter2.obj;
		}

		// XXX: Mesh ray direction test
		//sassert(ninside >= 0);
		if (ninside < 0)
			ninside = 0;

		// TODO: MAX{r.density, for r in all inside rends}
		if (rend_in == NULL) // currently inside an object emit to air
		{
			refract_rindex = REFRACTIVE_INDEX_AIR;
			refract_density = DENSITY_AIR;
		}
		else 
		{
			refract_rindex = rend_in->material->refractive_index;
			refract_density = rend_in->material->density;

		}

		sassert(refract_rindex > EPS);
#else

		refract_density = obj->material->density;
		refract_rindex = obj->material->refractive_index;
#endif

		Vector refract_dir = getRefract(
				ray.dir, normal, 
				refractive_index, 
				refract_rindex);

		if (refract_dir.lengthsqr() < EPS) // whole reflection
			break;

		Ray refract_ray(intersection, refract_dir);


		refract_intensity = 
			rayTracing(refract_ray, refract_rindex,
					refract_energy, refract_density,
					ninside, depth + 1, scene);

	} while (0);

	// blend by energy coeficient
	// @intensity = ambient + diffuse + specular
	return (light_intensity * surface->energy.light 
			+ reflect_intensity * surface->energy.reflect
			+ refract_intensity * surface->energy.refract);// * energy;

}

Vector RayTracing::getReflect(
		const Vector &incidence,
		const Vector &normal)
{
	sassert(fabs(incidence.lengthsqr() - 1) < EPS);

	sassert(fabs(normal.lengthsqr() - 1) < EPS);

#ifdef __DEBUG_BUILD
	Vector reflect;

	reflect = incidence - 2 * incidence.dot(normal) * normal;

	sassert(fabs(reflect.lengthsqr() - 1) < EPS);
	sassert(fabs((-incidence - normal).lengthsqr() - (reflect - normal).lengthsqr()) < EPS);
	return reflect;
#else
	return incidence - 2 * incidence.dot(normal) * normal;
#endif
}

Vector RayTracing::getRefract(
		const Vector &incidence,
		const Vector &normal,
		real_t rindex_in,
		real_t rindex_out)
{

	sassert(fabs(normal.lengthsqr() - 1) < EPS);
	sassert(fabs(incidence.lengthsqr() - 1) < EPS);
	sassert(rindex_in > EPS && rindex_out > EPS);
	sassert(incidence.dot(normal) < -EPS);

	real_t cos_i = -incidence.dot(normal);

	sassert(cos_i < 1 + EPS && cos_i > EPS);

	real_t delta = 1
		- sqr(rindex_in) * (1 - cos_i * cos_i)
		/ (rindex_out * rindex_out);

	// XXX
	if (delta < 0)
		return Vector(0, 0, 0);

	sassert(delta > 0);

	real_t cos_t = sqrt(delta);

	sassert(fabs(cos_t) < 1 + EPS);

	real_t ratio = rindex_in / rindex_out;

	Vector refract;

	refract = ratio * incidence + (ratio * (-incidence.dot(normal)) - cos_t) * normal;

	refract.unitize();
	sassert(refract.dot(incidence) > EPS);
	return refract;
}

